import { AppRegistry } from 'react-native';
import App from './src/mobile/App';

AppRegistry.registerComponent('dialo_mobile', () => App);
