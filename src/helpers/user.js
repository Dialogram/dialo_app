import Immutable from 'immutable';

export default class UserHelper {
  static getUserId(state) {
    if (!Immutable.Map.isMap(state.user)) { return null; }
    const user = state.user.get('user');
    if (!user || !Immutable.Map.isMap(user)) {
      return null;
    }
    return user.first().get('id');
  }

  static getUserProfile(state) {
    const user = state.user.get('user');
    if (!user || !Immutable.Map.isMap(user)) {
      return null;
    }
    return user.first().get('profile');
  }

  static getUserNickname(state) {
    const user = state.user.get('user');
    if (!user || !Immutable.Map.isMap(user)) {
      return null;
    }
    return user.first().get('nickName');
  }

  static getUserFirstName(state) {
    const user = state.user.get('user');
    if (!user || !Immutable.Map.isMap(user)) {
      return null;
    }
    return user.first().get('profile').get('firstName');
  }

  static getUserLastName(state) {
    const user = state.user.get('user');
    if (!user || !Immutable.Map.isMap(user)) {
      return null;
    }
    return user.first().get('profile').get('lastName');
  }

  static getUserEmail(state) {
    const user = state.user.get('user');
    if (!user || !Immutable.Map.isMap(user)) {
      return null;
    }
    return user.first().get('email');
  }
}
