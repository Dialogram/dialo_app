import Immutable from 'immutable';

export default class DocumentHelper {
  static getSelectedForDoc(state, idDocument) {
    const selected = state.documents.get(idDocument);
    if (!selected || !Immutable.Map.isMap(selected)) {
      return null;
    }
    return selected.get('selected').toJS();
  }

  static getConfirmedForDoc(state, idDocument) {
    const selected = state.documents.get(idDocument);
    if (!selected || !Immutable.Map.isMap(selected)) {
      return null;
    }
    return selected.get('confirmed').toJS();
  }

  static getDocumentById(state, id) {
    const doc = state.documents.get('documents');

    if (!doc || !Immutable.Map.isMap(doc)) {
      return null;
    }

    if (doc.get(id)) {
      return doc.get(id).toJS();
    }
    return null;
  }

  static getAllDocument(state) {
    const doc = state.documents.get('documents');
    if (!doc || !Immutable.Map.isMap(doc)) {
      return null;
    }
    return doc.toJS();
  }

  static getTranslationForDoc(state, id) {
    if (!state || !state.documents || !id) {
      return null;
    }

    let idTranslation = state.documents.get('documents');
    if (idTranslation === undefined) {
      return null;
    }

    idTranslation = idTranslation.get(id);
    if (idTranslation === undefined) {
      return null;
    }

    idTranslation = idTranslation.get('idTranslation');
    if (idTranslation === undefined) {
      return null;
    }

    let translation = state.documents.get('translations');
    if (translation === undefined || translation === null) {
      return null;
    }

    translation = translation.get(`${idTranslation}`);
    if (translation === null || translation === undefined) {
      return null;
    }
    return translation.toJS();
  }
}
