import Immutable from 'immutable';
// import { VIDEOS } from 'flux/actions/videos';
import store from '../flux/redux';
export default class VideoHelper {
  static getAllVideo(state) {
    if (!state || !state.video) { return null; }
    const selected = state.video.get('videos');

    if (!selected) {
      return null;
    }

    console.log('VIDEOOOOOO=>', selected.toJS());
    return selected.toJS();
  }

  static getAccessToken() {
    const session = store.getState().session.get('video');
    if (!session || !Immutable.Map.isMap(session)) {
      return null;
    }
    return `${session.first().get('access_token')}`;
  }

  static getHeaderAuthorization() {
    const session = store.getState().session.get('video');
    if (!session || !Immutable.Map.isMap(session)) {
      return null;
    }
    return `Bearer ${session.first().get('access_token')}`;
  }

  static getTmpObjectVideoEndpoint(state) {
    var selected = state.video.get('tmpvideoobject');
    if (selected) { selected = selected.first(); }
    if (selected) { selected = selected.get('source'); }
    if (!selected) { return null; }
    return selected.toJS();
  }
}
