import { SCOPE } from '../flux/actions/documents';

export default class UiHelper {
  static getDeleteConfirmWindows(state) {
    const ui = state.ui.get(SCOPE.confirmRemoveTranslation);
    let notif;
    if (ui !== undefined && ui !== null) {
      return ui;
    } else { notif = null; }
    return notif;
  }
}
