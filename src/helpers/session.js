import store from '../flux/redux';
import Immutable from 'immutable';
// import { removeSession } from '../flux/actions/session';
import NavigationFlux from '../flux/navigation';

export default class SessionHelper {
  static deleteSessionData() {
    //  store.dispatch(removeSession());
    NavigationFlux.navigate('auth', 'replace');
  }

  static hasSessionActive() {
    return !!store.getState().session.get('session');
  }

  static getHeaderAuthorization() {
    const session = store.getState().session.first();
    if (!session || !Immutable.Map.isMap(session)) {
      return null;
    }
    return `Bearer ${session.first().get('token')}`;
  }

  static getSessionId(state) {
    const session = state.session.first();
    if (!session || !Immutable.Map.isMap(session)) {
      return null;
    }
    return session.first().get('id');
  }
}
