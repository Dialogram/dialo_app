import { SESSION } from '../flux/actions/session';
import { SCOPE, DOCUMENT, TRANSLATION } from '../flux/actions/documents';

export default class NotificationHelper {
  static notifGetDocumentTranslation(state) {
    const notification = state.notification.get(TRANSLATION);
    if (notification === undefined || notification === null) { console.log('notifGetDocument is null'); return null; }
    console.log('NotifGetDocumentTranslation is not null', notification.get(SCOPE.getDocumentTranslation));
    return notification.get(SCOPE.getDocumentTranslation);
  }

  // ex
  static getSessionNotif(state) {
    const notification = state.notification.get(SESSION.SESSION);
    if (notification === undefined || notification === null) { return null; }
    return notification.get(SESSION.SESSION);
  }

  static getUserNotif(state) {
    const notification = state.notification.get(SESSION.SESSION);
    if (notification === undefined || notification === null) { return null; }
    return notification.get(SESSION.USER);
  }

  static getDeleteTranslateConfirmation(state) {
    const notification = state.notification.get(DOCUMENT);
    let notif;
    if (notification !== undefined) { notif = notification.get(SCOPE.confirmRemoveTranslation); } else { notif = null; }
    return notif;
  }

  static getDocumentNotifById(state, id) {
    const notification = state.notification.get(DOCUMENT);
    let notif;
    if (notification !== undefined) { notif = notification.get(id); } else { notif = null; }
    return notif;
  }

  static getPostDocumentNotif(state) {
    const notification = state.notification.get(DOCUMENT);
    let notif;
    if (notification !== undefined) { notif = notification.get(SCOPE.postDocument); } else { notif = null; }
    return notif;
  }
}
