import React, { Component } from 'react';
import { history, persistor, loadPersistStore } from '../flux/redux';
import { Route, Switch } from 'react-router';
import { ConnectedRouter } from 'connected-react-router';
import PrivateRoute from './navigation/privateRoute';
import { connect } from 'react-redux';
import NotFound from './navigation/notFound';
import { bindActionCreators } from 'redux';
import SessionHelper from '../flux/Session/Selector';
import { getUser, resendConfirmToken } from '../flux/User/Actions';
import { PersistGate } from 'redux-persist/integration/react';

import '../css/index.js';

import Auth from './views/Auth';
import Settings from './views/Profile';
import Register from './views/Register';
import Home from './views/Home';
import TraductionCreate from './views/Traduction/TraductionCreate';
import TraductionView from './views/Traduction/TraductionView';
import ForgetPasswordLandingPage from './views/ForgetPasswordLandingPage';
import Search from './views/Search';

import { getAllDocument } from '../flux/Documents/Actions';
import { getAllVideoTranslation } from '../flux/TranslationVideo/Actions';
import UserHelper from '../flux/User/Selector';

import ExplorerView from './components/explorer/Explorer';

import Header from './components/Header';

import ConfirmAccount from './views/ConfirmAccount';
import ActiveUser from './components/ActiveUser';
import DocumentHelper from '../flux/Documents/Selector';


class Wrapper extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      session: "Default",
    }

    loadPersistStore(() => {
      this.setState({ ...this.state, loading: false })
    });
  }


  componentDidUpdate(prevProps) {
    if (SessionHelper.hasSessionActive()) {
      if (this.props.userStatus === "Default") {
        this.props.getUser();
      }
    }
  }

  render() {
    if (this.state.loading === true) {
      return (
        <div className={"loader-container"}>
          <div className={'loader-item'}>
            <div className="dbl-spinner"></div>
            <div className="dbl-spinner"></div>
          </div>

        </div>
      )
    }
    let error = this.props.getUserError.toJS();
    return (
      <PersistGate loading={<div>loading</div>} persistor={persistor}>
        <ConnectedRouter history={history}>
          {error.code === 403 && error.message === "User should validate his email address" ? <ActiveUser resendConfirmToken={this.props.resendConfirmToken} /> : null}
          <div className="root-container">

            <div className="body-container">
              <div className="root-home-container">

                <div
                  className={'root-home-container'}>
                  <div className="home-header-container">
                    <Header />
                  </div>
                  <Switch>
                    <PrivateRoute exact path={'/'} component={Home} userStatus={this.props.userStatus} />
                    <PrivateRoute exact path={'/traduction/view/:id?'} component={TraductionView} />
                    <PrivateRoute exact path={'/traduction/create/:id?'} component={TraductionCreate} />
                    <PrivateRoute path={'/settings'} component={Settings} />
                    <PrivateRoute exact path={'/search'} component={Search} />
                    <Route path={'/auth'} component={Auth} />
                    <Route path={'/register'} component={Register} />
                    <Route path={'/password/forget/:token'} component={ForgetPasswordLandingPage} />
                    <Route path={'/explorer'} component={ExplorerView} />
                    <Route path={'/account/confirm/:token'} component={ConfirmAccount} />
                    <Route component={NotFound} />
                  </Switch>
                </div></div>
            </div>
          </div>
        </ConnectedRouter>
      </PersistGate>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    hasSessionActive: SessionHelper.hasSessionActive(),
    sessionStatus: SessionHelper.getSessionStatus(),
    userStatus: UserHelper.getUserStatus(),
    getUserError: UserHelper.getUserError(),
    getDocumentStatus: DocumentHelper.getDocumentStatus()
  };
};

const mapDispatchToPropsTransient = (dispatch) => {
  return {
    getUser: bindActionCreators(getUser, dispatch),
    getAllDocument: bindActionCreators(getAllDocument, dispatch),
    getAllVideoTranslation: bindActionCreators(getAllVideoTranslation, dispatch),
    resendConfirmToken: bindActionCreators(resendConfirmToken, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToPropsTransient)(Wrapper);
