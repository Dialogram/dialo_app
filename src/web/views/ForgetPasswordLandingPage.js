import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { forgotPasswordConfirm } from '../../flux/Session/Actions';
import FormErrors from '../components/FormErrors';
import Utils from '../helpers/Utils';

import SessionHelper from '../../flux/Session/Selector';

class ForgetPasswordLandingPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      password: '',
      password2: '',
      disabled: true,
      showError: false,
      formErrors: { password: '', password2: '' },
      passwordValid: false,
      password2Valid: false,
      isRequestSent: false,
    };
    this._handleInputChange = this._handleInputChange.bind(this);
    this._validateField = this._validateField.bind(this);
    this._onBlur = this._onBlur.bind(this);
    this._handleEnterKeyPress = this._handleEnterKeyPress.bind(this);
    this.sendPassword = this.sendPassword.bind(this);
  }

  sendPassword() {
    const { match: { params } } = this.props;
    return (<form className="auth-form">
      <div className="auth-input-container">
        <input className="auth-input"
          placeholder="Mot de passe"
          value={this.state.password}
          name="password"
          id={'password'}
          type={'password'}
          onKeyPress={this._handleEnterKeyPress}
          onChange={this._handleInputChange}
          onBlur={this._onBlur} />
        <input className="register-input"
          placeholder="Confirmation de mot de passe"
          value={this.state.password2}
          name="password2"
          id={'password2'}
          type={'password'}
          onKeyPress={this._handleEnterKeyPress}
          onBlur={this._onBlur}
          onChange={this._handleInputChange}
        />
        <FormErrors fieldName='password2' message={this.state.formErrors.password2} />
        <FormErrors fieldName='password' message={this.state.formErrors.password} />
        <FormErrors fieldName='password' message={
          this.state.isRequestSent === true &&
                        this.props.sessionError.code === 401
            ? "Impossible de réinitialiser votre mot de passe. Assurez vous d'avoir utiliser le lien fourni dans l'email." : null} />
        <button className="auth-button" onClick={(e) => { this.props.forgotPasswordConfirm(params.token, this.state.password); e.preventDefault(); }} disabled={this.state.disabled}>Confirmer</button>
      </div>
    </form>);
  }
  render() {
    return (
      <div className="root-auth-container">
        <div className="auth-form-container">
          <div className="auth-header-container">

            <img
              alt={'logo'}
              className={'auth-header-logo'}
              src={require('image/2020_logo_DIALOGRAM.png')}
            />
            <h1 className="auth-title">Nouveau mot de passe</h1>
          </div>
          <div className="auth-container">
            {this.sendPassword()}
          </div>
        </div>
      </div>
    );
  }
  _handleEnterKeyPress(e) {
    const { match: { params } } = this.props;
    if (e.key === 'Enter') {
      this._onBlur(e);
      if (this.state.disabled === false) {
        this.props.forgotPasswordConfirm(params.token, this.state.password);
      }
    }
  }

  _handleInputChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }

  _onBlur(e) {
    this._validateField(e.target.name, e.target.value);
  }

  _validateField(name, value) {
    let fieldValidationErrors = this.state.formErrors;
    let passwordValid = this.state.passwordValid;
    let password2Valid = this.state.password2Valid;
    switch (name) {
      case 'password':
        passwordValid = value.length >= 8;
        fieldValidationErrors.password = passwordValid ? '' : Utils.ERRORS['PassLen'];
        break;
      case 'password2':
        password2Valid = value === this.state.password;
        fieldValidationErrors.password2 = password2Valid ? '' : Utils.ERRORS['SamePass'];
        break;
      default:
        break;
    }
    this.setState({
      formErrors: fieldValidationErrors,
      passwordValid: passwordValid,
      password2Valid: password2Valid,
    }, this.validateForm);
  };

  validateForm() {
    this.setState({ disabled: !(this.state.passwordValid && this.state.password2Valid) });
  };
}
const mapStateToProps = (state, ownProps) => {
  return {
    sessionStatus: SessionHelper.getSessionStatus(),
    sessionError: SessionHelper.getSessionError(),
  };
};

const mapDispatchToPropsTransient = (dispatch) => {
  return {
    forgotPasswordConfirm: bindActionCreators(forgotPasswordConfirm, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToPropsTransient)(ForgetPasswordLandingPage);
