import React, { Component } from 'react';
import { connect } from 'react-redux';
import Home from './Home';

class Explore extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <div className={'root-home-container'}>
        <Home>

        </Home>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {

  };
};

const mapDispatchToPropsTransient = (dispatch) => {
  return {

  };
};

export default connect(mapStateToProps, mapDispatchToPropsTransient)(Explore);
