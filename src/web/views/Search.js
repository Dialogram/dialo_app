import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import '../../css/search/search.css';
import { getAllDocument, getDocumentByCat } from '../../flux/Documents/Actions';
import DocumentHelper from '../../flux/Documents/Selector';
import Header from '../components/Header';

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <div className={'root-home-container'}>
        <Header/>
        <div className="page-container">
          <div className="dashboard-left-search">
            <div className="category-list-container">
              <label className="category-label">Catégories</label>
              <ul className="category-list-items">
                <li className="list-item" onClick={undefined}>
                  <i className="material-icons category-icon">account_balance</i>
                  <a>Administratif</a>
                </li>
                <li className="list-item" onClick={undefined}>
                  <i className="material-icons category-icon">insert_emoticon</i>
                  <a>Divertissement</a>
                </li>
                <li className="list-item" onClick={undefined}>
                  <i className="material-icons category-icon">work_outline</i>
                  <a>Entreprise</a>
                </li>
                <li className="list-item" onClick={undefined}>
                  <i className="material-icons category-icon">monetization_on</i>
                  <a>Finance</a>
                </li>
                <li className="list-item" onClick={undefined}>
                  <i className="material-icons category-icon">favorite_border</i>
                  <a>Santé</a>
                </li>
              </ul>
            </div>
          </div>
          <div className="dashboard-middle">
            <div className="search-results-container">
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    documentList: DocumentHelper.getAllDocument(state),
    documentsStatus: DocumentHelper.getDocumentStatus(state),
  };
};

const mapDispatchToPropsTransient = (dispatch) => {
  return {
    getAllDocument: bindActionCreators(getAllDocument, dispatch),
    getDocumentByCat: bindActionCreators(getDocumentByCat, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToPropsTransient)(Search);
