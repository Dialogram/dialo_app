import React, { Component } from 'react';
import { connect } from 'react-redux';
import UserHelper from '../../flux/User/Selector';
import DocumentHelper from '../../flux/Documents/Selector';
import VideoHelper from '../../flux/Videos/Selector';
import { bindActionCreators } from 'redux';
import { getAllDocument } from '../../flux/Documents/Actions';
import { getAllVideo } from '../../flux/Videos/Actions';
import Home from './Home';

class Traduction extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.props.getAllDocument();
    this.props.getAllVideo();
  }

  render() {
    return (
      <div className={'root-home-container'}>
        <Home>
          <div>hello world</div>
        </Home>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    nickName: UserHelper.getUserNickname(state),
    userEmail: UserHelper.getUserEmail(state),
    documentList: DocumentHelper.getAllDocument(state),
    videoList: VideoHelper.getVideoList(state),
  };
};

const mapDispatchToPropsTransient = (dispatch) => {
  return {
    getAllDocument: bindActionCreators(getAllDocument, dispatch),
    getAllVideo: bindActionCreators(getAllVideo, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToPropsTransient)(Traduction);
