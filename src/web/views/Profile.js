import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Route, Link } from 'react-router-dom';

import 'css/profile/profile.css';

import UserHelper from 'flux/User/Selector';
import PublicProfileForm from 'web/components/profile/PublicProfileForm';
import PrivateProfileForm from 'web/components/profile/PrivateProfileForm';
import PasswordForm from 'web/components/profile/PasswordForm';
import PictureProfileHandler from 'web/components/profile/PictureProfileHandler';
import DeleteAccount from 'web/components/profile/DeleteAccount';

import { AvatarBig, AvatarMedium } from 'web/components/avatar';

import PopupWindow from 'web/components/PopupWindow';
const CATEGORIES = [
  { key: 'public', value: 'Informations publiques' },
  { key: 'picture', value: 'Gestion de la photo de profil' },
  { key: 'account', value: 'Informations privées' },
  { key: 'password', value: 'Mot de passe' },
  { key: 'delete', value: 'Suppression du compte' },
];

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      publicInfo: false,
      privateInfo: false,
      avatar: false,
      mdp: false,
      account: false
    };
  }

  closePublique = () => {
    this.setState({
      ...this.state,
      publicInfo: false
    })
  }
  openPublique = () => {
    this.setState({
      ...this.state,
      publicInfo: true
    })
  }
  closePrivate = () => {
    this.setState({
      ...this.state,
      privateInfo: false
    })
  }
  openPrivate = () => {
    this.setState({
      ...this.state,
      privateInfo: true
    })
  }

  openMdp = (e) =>{
    e.preventDefault();
    this.setState({
      ...this.state,
      privateInfo: true
    })
  }
  closeMdp = () =>{
    this.setState({
      ...this.state,
      privateInfo: false
    })
  }
  openAccount = (e) =>{
    e.preventDefault();
    this.setState({
      ...this.state,
      account: true
    })
  }
  closeAccount = () =>{
    this.setState({
      ...this.state,
      account: false
    })
  }
  render() {
    const listCategories = props => CATEGORIES.map(function (category) {
      const className = `${props.location.pathname === `${props.match.url}/${category.key}` ? 'linkActive' : 'link'}`;
      return <li key={category.key}>
        <Link to={`${props.match.url}/${category.key}`} className={className}>{category.value}</Link>
        <i className="material-icons">keyboard_arrow_right</i>
      </li>;
    });
    return (


      <div className="page-container">
        <PopupWindow title={'Modifier les informations publiques'} close={this.closePublique} isOpen={this.state.publicInfo}>
          <PublicProfileForm />

        </PopupWindow>
        <PopupWindow title={'Modifier les informations privés'} close={this.closePrivate} isOpen={this.state.privateInfo}>
          <PrivateProfileForm />

        </PopupWindow>
        <PopupWindow title={'Modifier le mot de passe'} close={this.closeMdp} isOpen={this.state.mdp}>
          <PasswordForm />

        </PopupWindow>
        <PopupWindow title={'Supprimer mon compte'} close={this.closeAccount} isOpen={this.state.account}>
          <PasswordForm />

        </PopupWindow>
        <div className="profil-container">
          <img src={this.props.profile.profilePicture.url} />
          <p className="identity">{`${this.props.firstName} ${this.props.lastName}`}</p>
          <p className={"description"}>{this.props.profile.description}</p>
          <div className={'identity-info'}>
            <div className={'identity-info-container'}>
              <p>Sexe</p>
              <p>{this.props.profile.gender ? this.props.profile.gender : "None"}</p>
            </div>
            <div className={'identity-info-container'}>
              <p>Date de naissance</p>
              <p>{this.props.profile.birthday ? this.props.profile.birthday : "None"}</p>
            </div>
            <div className={'identity-info-container'}>
              <p>Certifié</p>
              <p>{this.props.profile.certified ? this.props.profile.certified : "Non"}</p>
            </div>
            <div className={'identity-info-container'}>
              <p>Ville</p>
              <p>{this.props.profile.hometown ? this.props.profile.hometown : "None"}</p>
            </div>
          </div>
          <div className={'profil-public-info'}>
            <p>Information publique <span id={"edite-info"} onClick={this.openPublique}>Editer</span></p>
            <div>
              <p>Nom : </p>
              <p>{this.props.firstName}</p>
            </div>
            <div>
              <p>Prénom : </p>
              <p>{this.props.lastName}</p>
            </div>
            <div>
              <p>Description : </p>
              <p>{this.props.profile.description ? this.props.profile.description : 'None'}</p>
            </div>
            <div>
              <p>Date de naissance : </p>
              <p>{this.props.profile.birthday ? this.props.profile.birthday : 'None'}</p>
            </div>
            <div>
              <p>Sexe : </p>
              <p>{this.props.profile.gender ? this.props.profile.gender : "None"}</p>
            </div>
            <div>
              <p>Ville : </p>
              <p>{this.props.profile.hometown ? this.props.profile.hometown : "None"}</p>
            </div>
            <div>
              <p>Pays : </p>
              <p>{this.props.profile.country ? this.props.profile.country : "None"}</p>
            </div>
          </div>
          <div className={'profil-public-info'}>
            <p>Information privé <span id={"edite-info"} onClick={this.openPrivate}>Editer</span></p>
            <div>
              <p>E-mail : </p>
              <p>{this.props.email}</p>
            </div>
            <div>
              <p>Pseudo: </p>
              <p>{this.props.nickName}</p>
            </div>
          </div>
          <div className={'profil-public-info'}>
            <p>Autre</p>
            <div>
              <button onClick={this.openMdp}>Changer mon mot de passe</button>
            </div>
            <div>
              <button onClick={this.openAccount}>Supprimer mon compte</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    nickName: UserHelper.getUserNickname(state),
    profile: UserHelper.getUserProfile(state),
    email: UserHelper.getUserEmail(state),
    firstName: UserHelper.getUserFirstName(state),
    lastName: UserHelper.getUserLastName(state)
  };
}

export default connect(mapStateToProps)(Profile);

/*

 <div className="settings-div">
          <AvatarBig />
          <ul className="settings-list">
            {listCategories(this.props)}
          </ul>
        </div>
        <div className="settings-content">
          <Route path={`${this.props.match.path}/public`} component={PublicProfileForm} />
          <Route path={`${this.props.match.path}/picture`} component={PictureProfileHandler} />
          <Route path={`${this.props.match.path}/account`} component={PrivateProfileForm} />
          <Route path={`${this.props.match.path}/password`} component={PasswordForm} />
          <Route path={`${this.props.match.path}/delete`} component={DeleteAccount} />
        </div>
        */