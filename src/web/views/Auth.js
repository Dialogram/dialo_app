import React, { Component } from 'react';
import '../../css/auth/auth.css';
import Footer from '../components/Footer';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { LoginFlow, forgotPassword, createSession } from '../../flux/Session/Actions';
import SessionHelper from '../../flux/Session/Selector';
import Utils from '../helpers/Utils';
import FormErrors from '../components/FormErrors';
import NavigationFlux from '../../flux/navigation.web';
import PopupWindow from '../components/PopupWindow';

class Auth extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      disabled: true,
      showError: false,
      formErrors: { email: '', password: '' },
      emailValid: false,
      passwordValid: false,
      popUpForgotPassword: false,
    };

    this._handleInputChange = this._handleInputChange.bind(this);
    this._signIn = this._signIn.bind(this);
    this._validateField = this._validateField.bind(this);
    this._onBlur = this._onBlur.bind(this);
    this._handleEnterKeyPress = this._handleEnterKeyPress.bind(this);
    this._openPopUpForgotPassword = this._openPopUpForgotPassword.bind(this);
    this._closePopUpForgotPassword = this._closePopUpForgotPassword.bind(this);
  }

  onClickRegisterLink() {
    NavigationFlux.navigate('register', 'replace');
  }

  render() {
    return (
      <div className="root-auth-container">
        <div className="auth-form-container">
          <div className="auth-header-container">
            <img
              alt={'logo'}
              className={'auth-header-logo'}
              src={require('image/2020_logo_DIALOGRAM.png')}
            />
            <h1 className="auth-title">Dialogram</h1>
          </div>
          <div className="auth-container">
            <PopupWindow title={'Récupération du mot de passe'} close={this._closePopUpForgotPassword} isOpen={this.state.popUpForgotPassword}>
              <div className="auth-form-container">
                <div className="auth-header-container">
                  <p> Saisissez votre e-mail pour récupérer votre mot de passe</p>
                  <form className="auth-form">
                    <div className="auth-input-container">
                      <input className="auth-input"
                        placeholder="Email"
                        value={this.state.email}
                        name="email"
                        id={'email'}
                        type={'email'}
                        onKeyPress={this._handleEnterKeyPress}
                        onChange={this._handleInputChange}
                        onBlur={this._onBlur}/>
                      <FormErrors fieldName='email' message={this.state.formErrors.email} />
                      <button className="auth-button" onClick={(e) => { this.props.forgotPassword(this.state.email); e.preventDefault(); this._closePopUpForgotPassword(e); }} disabled={this.state.disabled}>Envoyez l&apos;email</button>
                    </div>
                  </form>
                </div>
              </div>
            </PopupWindow>
            <form className="auth-form">
              <div className="auth-input-container">

                <input className="auth-input"
                  placeholder="Email"
                  value={this.state.email}
                  name="email"
                  id={'email'}
                  type={'email'}
                  onKeyPress={this._handleEnterKeyPress}
                  onChange={this._handleInputChange}
                  onBlur={this._onBlur} />
                <FormErrors fieldName='email' message={this.state.formErrors.email} />

                <input className="auth-input"
                  placeholder="Mot de passe"
                  value={this.state.password}
                  name="password"
                  id={'password'}
                  type={'password'}
                  onKeyPress={this._handleEnterKeyPress}
                  onChange={this._handleInputChange}
                  onBlur={this._onBlur} />
                <FormErrors fieldName='password' message={this.state.formErrors.password} />

                <button className="auth-button" onClick={this._signIn} disabled={this.state.disabled}>Se connecter</button>

                <a onClick={this._openPopUpForgotPassword} className="forgot-password-link">Mot de passe oublié ?</a>
              </div>
            </form>
          </div>
        </div>
        <div className="register-link-container">
          <a onClick={this.onClickRegisterLink} className="register-link">Vous n&apos;avez pas encore de compte ?</a>
        </div>
        <div className="get-app-container">
          <p className="get-app-p">Téléchargez l&apos;application</p>
          <div className="get-app-images">
            <img
              alt={'GooglePlay'}
              className="get-it-on-google-play"
              src={require('image/get-it-on-google-play.png')}
            />
          </div>
        </div>
        <div className="footer-component">
          <Footer />
        </div>
      </div>
    );
  }

  _closePopUpForgotPassword(e) {
    this.setState({
      ...this.state,
      popUpForgotPassword: false,
    });
    e.preventDefault();
  }
  _openPopUpForgotPassword(e) {
    this.setState({
      ...this.state,
      popUpForgotPassword: true,
    });
    e.preventDefault();
  }

  _handleEnterKeyPress(e) {
    if (e.key === 'Enter') {
      this._onBlur(e);
      if (this.state.disabled === false) {
        this._signIn(e);
      }
    }
  }

  _handleInputChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }

  _onBlur(e) {
    this._validateField(e.target.name, e.target.value);
  }

  _validateField(name, value) {
    let fieldValidationErrors = this.state.formErrors;
    let emailValid = this.state.emailValid;
    let passwordValid = this.state.passwordValid;

    switch (name) {
      case 'email':
        emailValid = value.match(Utils.EMAIL_REGEX);
        fieldValidationErrors.email = emailValid ? '' : Utils.ERRORS['WrongEmail'];
        break;
      case 'password':
        passwordValid = value.length >= 8;
        fieldValidationErrors.password = passwordValid ? '' : Utils.ERRORS['PassLen'];
        break;
      default:
        break;
    }
    this.setState({
      formErrors: fieldValidationErrors,
      emailValid: emailValid,
      passwordValid: passwordValid,
    }, this.validateForm);
  };

  validateForm() {
    this.setState({ disabled: !(this.state.emailValid && this.state.passwordValid) });
  };

  _signIn(e) {
    const { from } = this.props.location.state || { from: { pathname: '/' } };
    this.props.createSession({
      email: this.state.email,
      password: this.state.password,
      from,
    });
    e.preventDefault();
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    sessionError: SessionHelper.getSessionError(),
    sessionStatus: SessionHelper.getSessionStatus(),
  };
};

const mapDispatchToPropsTransient = (dispatch) => {
  return {
    LoginFlow: bindActionCreators(LoginFlow, dispatch),
    createSession: bindActionCreators(createSession, dispatch),
    forgotPassword: bindActionCreators(forgotPassword, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToPropsTransient)(Auth);
