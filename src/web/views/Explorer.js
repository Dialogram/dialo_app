import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import '../../css/home/home.css';
import { getAllDocument, exploreDocument } from '../../flux/Documents/Actions';
import DocumentHelper from '../../flux/Documents/Selector';
// import NavigationFlux from '../../flux/navigation.web';
import { getAllVideoTranslation } from '../../flux/TranslationVideo/Actions';
import VideoTranslationHelper from '../../flux/TranslationVideo/Selector';
import VideoHelper from '../../flux/Videos/Selector';
import { getUser } from '../../flux/User/Actions';
import UserHelper from '../../flux/User/Selector';
// import AddFile from '../components/AddFile';
// import { DropDown } from '../components/DropDown';
// import Header from '../components/Header';
// import PopupWindow from '../components/PopupWindow';

import DashboardLeft from '../components/DashboardLeft';

import Explorer from '../components/explorer/Explorer';

import {
  removeVideoFlow,
} from '../../flux/Videos/Actions';

class ExplorerView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      popUpAddFile: false,

    };
  }

  componentDidMount() {
    // this.props.getUser();
    this.props.getAllDocument();
    this.props.getAllVideoTranslation();
  }

  render() {
    if (this.props.profile === null || this.props.profile === undefined) {
      return (
        <div>
          Chargement..
        </div>
      );
    }
    return (

      <div className="page-container">
        <DashboardLeft />

        <div className="dashboard-middle">

          <Explorer />

        </div>
      </div>

    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    value: state.session,
    nickName: UserHelper.getUserNickname(state),
    userEmail: UserHelper.getUserEmail(state),
    documentList: DocumentHelper.getAllDocument(state),
    documentsStatus: DocumentHelper.getDocumentStatus(state),
    getVideoTranslationList: VideoTranslationHelper.getVideoTranslationList(state),
    videoList: VideoHelper.getVideoList(state),
    profile: UserHelper.getUserProfile(state),
    video: VideoTranslationHelper.getVideoTranslationById({ id: '5cbd59dcaa762503402ef569' }),

    getExploreDocument: DocumentHelper.getExploreDocument(),
  };
};

const mapDispatchToPropsTransient = (dispatch) => {
  return {
    getUser: bindActionCreators(getUser, dispatch),
    getAllDocument: bindActionCreators(getAllDocument, dispatch),
    getAllVideoTranslation: bindActionCreators(getAllVideoTranslation, dispatch),
    removeVideoFlow: bindActionCreators(removeVideoFlow, dispatch),

    exploreDocument: bindActionCreators(exploreDocument, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToPropsTransient)(ExplorerView);
