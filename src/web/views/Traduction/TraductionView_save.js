import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import '../../../css/traduction/traduction.css';
import { deleteDocumentFlow, documentFlowById, getDocumentById } from '../../../flux/Documents/Actions';
import DocumentHelper from '../../../flux/Documents/Selector';
import { createDocumentTranslationFlow, getDocumentTranslation } from '../../../flux/Translations/Actions';
import TraductionHelper from '../../../flux/Translations/Selector';
import { deleteVideoTranslationById } from '../../../flux/TranslationVideo/Actions';
import VideoTranslationHelper from '../../../flux/TranslationVideo/Selector';
import VideoHelper from '../../../flux/Videos/Selector';
// import { CustomIconButton } from '../../components/common/CustomIconButton';
import ReactPlayer from 'react-player';

import DashboardLeft from '../../components/DashboardLeft';
import PdfView from './PdfView_ex';

class TraductionView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      deleteDocument: false,
      popUpEditDocument: false,
      numPages: null,
      pageNumber: 1,
    };

    this.closeEditDocument = this.closeEditDocument.bind(this);
    this.openDocumentDetails = this.openDocumentDetails.bind(this);
    this.openValidateDeleteDoc = this.openValidateDeleteDoc.bind(this);
    this.closeValidateDeleteDoc = this.closeValidateDeleteDoc.bind(this);
    this.nextOrPrevPage = this.nextOrPrevPage.bind(this);
    this._handleDeleteDocument = this._handleDeleteDocument.bind(this);
    this._handleDeleteVideoTranslation = this._handleDeleteVideoTranslation.bind(this);
  }

  componentDidMount() {
    this.props.documentFlowById({ id: this.props.match.params.id });
    // document.addEventListener('keydown', this.nextOrPrevPage, false);
  }

  nextOrPrevPage(e) {
    if (e.keyCode === 37) { this.goToPrevPage(); }
    if (e.keyCode === 39) { this.goToNextPage(); }
  }

  goToPrevPage = () =>
    this.setState(state => ({ pageNumber: state.pageNumber !== 1 ? state.pageNumber - 1 : state.pageNumber }));

  goToNextPage = () =>
    this.setState(state => ({ pageNumber: state.pageNumber !== this.state.numPages ? state.pageNumber + 1 : state.pageNumber }));

  closeEditDocument() {
    this.setState({
      popUpEditDocument: false,
    });
  }

  openDocumentDetails() {
    this.setState({
      popUpEditDocument: true,
    });
  }

  openValidateDeleteDoc() {
    this.setState({
      deleteDocument: true,
    });
  }

  closeValidateDeleteDoc() {
    this.setState({
      deleteDocument: false,
    });
  }

  render() {
    return (
      <div className="page-container">
        <DashboardLeft />

        <div className="dashboard-middle">
          {this.props.document &&
          <PdfView
            document={this.props.document}
            docTraduction={this.props.docTraduction}
            deleteDocumentFlow={this.props.deleteDocumentFlow}
            createDocumentTranslationFlow={this.props.createDocumentTranslationFlow}
          />}
          {this.props.videoTranslation &&
          <div>
            <div className="pdf-root-container">
              <div className="middle-dashboard-pdf">
                <div className="react-player-container">
                  <ReactPlayer url={this.props.videoTranslation.assets.hls} controls />
                </div>
                {/*                <div className="right-control-panel">

                  <div className="custom-button-item">
                    <CustomIconButton
                      onClickElement={this._handleDeleteVideoTranslation}
                      iconClass="material-icons icon-trad"
                      iconName="delete_outline"
                      label="Supprimer cette vidéo" />
                  </div>
                </div> */}
              </div>
            </div>
          </div>
          }
        </div>
      </div>
    );
  }

  _doNothing() {
    return true;
  }

  _handleDeleteDocument() {
    this.props.deleteDocumentFlow(this.props.docId);
    this.closeValidateDeleteDoc();
  }

  _handleDeleteVideoTranslation() {
    this.props.deleteVideoTranslation({ id: this.props.videoTranslation.id });
  }
}

const mapStateToProps = (state, ownProps) => {
  const currentId = ownProps.match.params.id;
  return {
    value: state.session,
    docId: currentId,
    document: DocumentHelper.getDocumentById(currentId),
    docTraduction: TraductionHelper.getTranslationForDoc(currentId),
    video: VideoHelper.getVideoById({ id: currentId }),
    videoTranslation: VideoTranslationHelper.getVideoTranslationById({ id: currentId }),
    getPullDocumentStatus: DocumentHelper.getPullDocumentStatus(),
  };
};

const mapDispatchToPropsTransient = (dispatch) => {
  return {
    getDocumentById: bindActionCreators(getDocumentById, dispatch),

    deleteVideoTranslation: bindActionCreators(deleteVideoTranslationById, dispatch),
    deleteDocumentFlow: bindActionCreators(deleteDocumentFlow, dispatch),
    getDocumentTranslation: bindActionCreators(getDocumentTranslation, dispatch),
    createDocumentTranslationFlow: bindActionCreators(createDocumentTranslationFlow, dispatch),
    documentFlowById: bindActionCreators(documentFlowById, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToPropsTransient)(TraductionView);
