import React, { Component } from 'react';
import { Document, Page, pdfjs } from 'react-pdf';
import '../../../css/Component/Component.css';
import '../../../css/index.css';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import '../../../css/traduction/traduction.css';
import { deleteDocumentById } from '../../../flux/Documents/Actions';
import DocumentHelper from '../../../flux/Documents/Selector';
import NavigationFlux from '../../../flux/navigation.web';
import {
  addTranslationInStore,
  createDocumentTranslationFlow,
  getDocumentTranslation,
  updateTranslation,
} from '../../../flux/Translations/Actions';
import TraductionHelper from '../../../flux/Translations/Selector';
import VideoTranslationHelper from '../../../flux/TranslationVideo/Selector';
import AccessRight from '../../components/AccessRight';
import { CustomIconButton } from '../../components/common/CustomIconButton';
import { ItemList } from '../../components/common/ItemList';

import PopupWindow from '../../components/PopupWindow';
import Home from '../Home';
import DragBox from './DragBox';
import Confirmation from '../../components/Confirmation';

pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;

class TraductionCreate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      test: false,
      deletionConfirmed: false,
      deleteZone: false,
      numPages: null,
      pageNumber: 1,
      active: false,
      selectedDoc: '',
      isBinding: false,
      selectedVideo: '',
      creatingZone: false,
      selectedTranslation: undefined,
      isZoneAndBindConfirmed: false,
      popupAccessRight: false,
    };

    this.selectTranslation = this.selectTranslation.bind(this);
    this.openDeleteZone = this.openDeleteZone.bind(this);
    this.closeDeleteZone = this.closeDeleteZone.bind(this);
    this.nextOrPrevPage = this.nextOrPrevPage.bind(this);
    this._isCreatingZone = this._isCreatingZone.bind(this);
    this._showVideoList = this._showVideoList.bind(this);
    this._onClickElement = this._onClickElement.bind(this);
    this._doNothing = this._doNothing.bind(this);
    this._handleDeleteZone = this._handleDeleteZone.bind(this);
  }

  onDocumentLoadSuccess = ({ numPages }) => {
    if (this.props.document.idTranslation) {
      this.props.getDocumentTranslation({
        id: this.props.document.id,
        idTranslation: this.props.document.idTranslation });
    }
    this.setState({ numPages, pageNumber: 1 });
  };

  componentDidMount() {
    document.addEventListener('keydown', this.nextOrPrevPage, false);
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.nextOrPrevPage, false);
  }

  nextOrPrevPage(e) {
    if (e.keyCode === 37) { this.goToPrevPage(); }
    if (e.keyCode === 39) { this.goToNextPage(); }
  }

  goToPrevPage = () =>
    this.setState(state => ({
      pageNumber: state.pageNumber !== 1
        ? state.pageNumber - 1
        : state.pageNumber,
    }));

  goToNextPage = () =>
    this.setState(state => ({
      pageNumber: state.pageNumber !== this.state.numPages
        ? state.pageNumber + 1
        : state.pageNumber,
    }));

  selectTranslation(e, id) {
    this.setState({
      ...this.state,
      selectedTranslation: undefined,
    }, () => {
      this.setState({
        ...this.state,
        selectedTranslation: id,
        itemToDelete: id.index,
      });
    });
  }

  onRightPopupClose = (e) => {
    this.setState({
      ...this.state,
      popupAccessRight: false,
    });
    e.preventDefault();
  };

  onRightPopupOpen = () => {
    this.setState({
      ...this.state,
      popupAccessRight: true,
    });
  };

  openDeleteZone = () => {
    console.log('open delete');
    this.setState({
      ...this.state,
      deleteZone: true,
    });
  };

  closeDeleteZone = () => {
    console.log('close delete');
    this.setState({
      deleteZone: false,
    });
  };

  _handleDeleteZone() {
    this.setState({
      ...this.state,
      deletionConfirmed: !this.state.deletionConfirmed,
    });
    this.closeDeleteZone();
  }

  render() {
    const { pageNumber, numPages } = this.state;

    return (
      <div className={'root-home-container'}>
        <PopupWindow
          title={'Supprimer une zone'}
          close={this.closeDeleteZone}
          isOpen={this.state.deleteZone}>
          <Confirmation
            boxTitle={'Êtes-vous sûr de vouloir supprimer cette zone ?'}
            deletionConfirmed={this._handleDeleteZone}
            deletionCanceled={this.closeDeleteZone} />
        </PopupWindow>

        <PopupWindow
          title={'Gérer les droits'}
          close={this.onRightPopupClose}
          isOpen={this.state.popupAccessRight}>
          <AccessRight
            doc={this.props.document}/>
        </PopupWindow>

        <Home>
          {(this.props.document !== undefined &&
            this.props.document !== null)
            ? <div className="pdf-root-container">
              <div className="middle-dashboard-pdf">
                <Document
                  className="document-pdf"
                  file={this.props.document.link}
                  onLoadSuccess={this.onDocumentLoadSuccess}>
                  <Page width={610} className="page-pdf" pageNumber={pageNumber}/>
                  {this.props.docTraduction &&
                    <DragBox
                      test={this.state.test}
                      deleteZone={this.state.deletionConfirmed}
                      creatingZone={this.state.creatingZone}
                      onClickTranslation={this.selectTranslation}
                      isZoneAndBindConfirmed={this.state.isZoneAndBindConfirmed}
                      selectedVideo={this.state.selectedVideo}
                      focus={this.state.selectedTranslation}
                      idDocument={this.props.docId}
                      docTraduction={this.props.docTraduction}
                      translation={this.props.docTraduction.translation}
                      pageNumber={pageNumber - 1}
                      setInStore={this.props.addTranslationInStore}/>
                  }
                </Document>
                {this.props.docTraduction !== null && this.props.docTraduction !== undefined
                  ? <div className="right-control-panel">
                    <div className="custom-button-item">
                      <CustomIconButton
                        onClickElement={() => {
                          NavigationFlux.navigate(`/traduction/view/${this.props.docId}`, 'replace');
                        }}
                        iconClass="material-icons icon-trad"
                        iconName="done_all"
                        label="Terminer la traduction"/>
                    </div>
                    <div className="button-helper-container">
                      <div className="custom-button-item">
                        <CustomIconButton
                          onClickElement={this._isCreatingZone}
                          iconClass="material-icons icon-trad"
                          iconName="photo_size_select_small"
                          label="Créer une zone à traduire"/>
                      </div>
                      {/* <div className="helper-container">
                        <i className="material-icons icon-help">help_outline</i>
                      </div> */}
                    </div>
                    <div className="hr-separator">
                      <hr/>
                    </div>
                    <div className="traduction-control-panel">
                      <div className="custom-button-item">
                        <CustomIconButton
                          onClickElement={this.openDeleteZone}
                          iconClass="material-icons icon-trad"
                          iconName="delete_outline"
                          label="Supprimer cette zone"/>
                      </div>
                      <div className="custom-button-item">
                        <CustomIconButton
                          onClickElement={this.onRightPopupOpen}
                          iconClass="material-icons icon-trad"
                          iconName="build"
                          label="Gérer les droits du document"/>
                      </div>
                      {this.state.selectedTranslation &&
                      <div className="custom-button-item">
                        <CustomIconButton
                          onClickElement={this._showVideoList}
                          iconClass="material-icons icon-trad"
                          iconName="link"
                          label="Lier cette zone à une vidéo"/>
                      </div>
                      }
                      {this.state.isBinding
                        ? <ItemList onClickElement={this._onClickElement} dataToShow={this.props.getVideoTranslationList}/> : undefined}
                    </div>
                  </div>
                  : <div/>
                }
              </div>
              <div className="controls-pdf-container">
                <button className="material-icons button-pdf-controls" onClick={this.goToPrevPage}>
                  keyboard_arrow_left
                </button>
                <p className="page-number-label">Page {pageNumber} sur {numPages}</p>
                <button className="material-icons button-pdf-controls" onClick={this.goToNextPage}>
                  keyboard_arrow_right
                </button>
              </div>
            </div>
            : <div>Chargement...</div>}
        </Home>
      </div>
    );
  }

  _doNothing() {
    this.setState({ isZoneAndBindConfirmed: true });
    return true;
  }

  _isCreatingZone() {
    this.setState({ creatingZone: !this.state.creatingZone });
  }

  _onClickElement(Video) {
    this.setState({ selectedVideo: Video, test: !this.state.test });
  }

  _showVideoList() {
    this.setState({ isBinding: !this.state.isBinding });
  }
}

const mapStateToProps = (state, ownProps) => {
  const id = ownProps.match.params.id;
  return {
    docId: id,
    document: DocumentHelper.getDocumentById(id),
    docTraduction: TraductionHelper.getTranslationForDoc(id),
    getVideoTranslationList: VideoTranslationHelper.getVideoTranslationList(state),
  };
};

const mapDispatchToPropsTransient = (dispatch) => {
  return {
    updateTranslation: bindActionCreators(updateTranslation, dispatch),
    deleteDocumentById: bindActionCreators(deleteDocumentById, dispatch),
    addTranslationInStore: bindActionCreators(addTranslationInStore, dispatch),
    getDocumentTranslation: bindActionCreators(getDocumentTranslation, dispatch),
    createDocumentTranslationFlow: bindActionCreators(createDocumentTranslationFlow, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToPropsTransient)(TraductionCreate);
