import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { deleteDocumentFlow, documentFlowById, getDocumentById } from '../../../flux/Documents/Actions';
import DocumentHelper from '../../../flux/Documents/Selector';
import { createDocumentTranslationFlow, getDocumentTranslation } from '../../../flux/Translations/Actions';
import TraductionHelper from '../../../flux/Translations/Selector';
import { deleteVideoTranslationById } from '../../../flux/TranslationVideo/Actions';
import VideoTranslationHelper from '../../../flux/TranslationVideo/Selector';
import SessionHelper from 'flux/Session/Selector';
import VideoHelper from '../../../flux/Videos/Selector';

import DashboardLeft from '../../components/DashboardLeft';

import PdfDisplay from './PdfDisplay';



import DragBox from './DragBox';
import CreateDocumentPanel from 'web/components/CreateDocumentPanel';
import { ItemList } from '../../components/common/ItemList';

import ForbidenAccess from '../../components/ForbidenAccess';

class TraductionCreate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      test: false,
      deletionConfirmed: false,
      deleteZone: false,
      numPages: null,
      pageNumber: 1,
      active: false,
      selectedDoc: '',
      isBinding: false,
      selectedVideo: '',
      creatingZone: false,
      selectedTranslation: undefined,
      isZoneAndBindConfirmed: false,
      popupAccessRight: false,
    };


  }

  onDocumentLoadSuccess = ({ numPages }) => {
    if (this.props.document.idTranslation) {
      this.props.getDocumentTranslation({
        id: this.props.document.id,
        idTranslation: this.props.document.idTranslation
      });
    }
    this.setState({ numPages, pageNumber: 1 });
  };

  componentDidMount() {
    this.props.documentFlowById({ id: this.props.match.params.id });
    // document.addEventListener('keydown', this.nextOrPrevPage, false);
  }

  componentWillUnmount() {
    //document.removeEventListener('keydown', this.nextOrPrevPage, false);
  }

  nextOrPrevPage = (e) => {
    if (e.keyCode === 37) { this.goToPrevPage(); }
    if (e.keyCode === 39) { this.goToNextPage(); }
  }

  goToPrevPage = () =>
    this.setState(state => ({
      pageNumber: state.pageNumber !== 1
        ? state.pageNumber - 1
        : state.pageNumber,
    }));

  goToNextPage = () =>
    this.setState(state => ({
      pageNumber: state.pageNumber !== this.state.numPages
        ? state.pageNumber + 1
        : state.pageNumber,
    }));

  _isCreatingZone = () => {
    this.setState({ creatingZone: !this.state.creatingZone });
  }

  _handleDeleteZone = () => {
    this.setState({
      ...this.state,
      deletionConfirmed: !this.state.deletionConfirmed,
    });
  }

  _showVideoList = () => {
    this.setState({ isBinding: !this.state.isBinding });
  }

  render() {
    if (this.props.document === null)
      return null;
    if (this.props.document.access.role !== "owner") {
      return (
        <div className="page-container">
          <div className="dashboard-middle">
            <ForbidenAccess />
          </div>
        </div>
      )
    }
    return (

      <div className="page-container">

        <DashboardLeft />

        <div className="dashboard-middle">
          <CreateDocumentPanel
            document={this.props.document}
            _isCreatingZone={this._isCreatingZone}
            _handleDeleteZone={this._handleDeleteZone}
            _showVideoList={this._showVideoList}
            docId={this.props.docId}
          />
          {
            this.props.document &&
            <PdfDisplay
              document={this.props.document}
              docTraduction={this.props.docTraduction}
              token={this.props.token}
            >
              <DragBox
                test={this.state.test}
                deleteZone={this.state.deletionConfirmed}
                creatingZone={this.state.creatingZone}
                onClickTranslation={this.selectTranslation}
                isZoneAndBindConfirmed={this.state.isZoneAndBindConfirmed}
                selectedVideo={this.state.selectedVideo}
                focus={this.state.selectedTranslation}
                idDocument={this.props.docId}
                docTraduction={this.props.docTraduction}
                translation={this.props.docTraduction !== null && this.props.docTraduction.translation}
                setInStore={this.props.addTranslationInStore}
                videoTranslation={this.props.getVideoTranslationList} />

            </PdfDisplay>
          }
          <div className="right-control-panel">
            {this.state.isBinding
              ? <ItemList onClickElement={this._onClickElement} dataToShow={this.props.getVideoTranslationList} /> : undefined}
          </div>
        </div>
      </div>

    );
  }

  _onClickElement = (Video) => {
    this.setState({ selectedVideo: Video, test: !this.state.test });
  }

  selectTranslation = (e, id) => {
    this.setState({
      ...this.state,
      selectedTranslation: undefined,
    }, () => {
      this.setState({
        ...this.state,
        selectedTranslation: id,
        itemToDelete: id.index,
      });
    });
  }
}

const mapStateToProps = (state, ownProps) => {
  const currentId = ownProps.match.params.id;
  return {
    token: SessionHelper.getHeaderAuthorization(),
    value: state.session,
    docId: currentId,
    document: DocumentHelper.getDocumentById(currentId),
    docTraduction: TraductionHelper.getTranslationForDoc(currentId),
    video: VideoHelper.getVideoById({ id: currentId }),
    videoTranslation: VideoTranslationHelper.getVideoTranslationById({ id: currentId }),
    getPullDocumentStatus: DocumentHelper.getPullDocumentStatus(),
    getVideoTranslationList: VideoTranslationHelper.getVideoTranslationList(state),
  };
};

const mapDispatchToPropsTransient = (dispatch) => {
  return {
    getDocumentById: bindActionCreators(getDocumentById, dispatch),
    deleteVideoTranslation: bindActionCreators(deleteVideoTranslationById, dispatch),
    deleteDocumentFlow: bindActionCreators(deleteDocumentFlow, dispatch),
    getDocumentTranslation: bindActionCreators(getDocumentTranslation, dispatch),
    createDocumentTranslationFlow: bindActionCreators(createDocumentTranslationFlow, dispatch),
    documentFlowById: bindActionCreators(documentFlowById, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToPropsTransient)(TraductionCreate);
