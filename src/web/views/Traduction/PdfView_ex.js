import React, { Component } from 'react';
import { Document, Page, pdfjs } from 'react-pdf';
import { connect } from 'react-redux';
import DisplayHLS from '../../components/DisplayHLS';

import DragBoxView from './DragBoxView';
import { CustomIconButton } from '../../components/common/CustomIconButton';

import NavigationFlux from '../../../flux/navigation.web';
import Confirmation from '../../components/Confirmation';
import EditDocumentDetails from '../../components/EditDocumentDetails';
import PopupWindow from '../../components/PopupWindow';

pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;

class PdfView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedVideo: undefined,
      deleteDocument: false,
      popUpEditDocument: false,
      numPages: null,
      pageNumber: 1,
    };

    this.doNothing = this.doNothing.bind(this);
  }
  componentDidMount() {
    document.addEventListener('keydown', this.nextOrPrevPage, false);
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.nextOrPrevPage, false);
  }

  onDocumentLoadSuccess = ({ numPages }) => {
    this.setState({ numPages, pageNumber: 1 });
  };

  nextOrPrevPage = (e) => {
    if (e.keyCode === 37) { this.goToPrevPage(); }
    if (e.keyCode === 39) { this.goToNextPage(); }
  };

  goToPrevPage = () =>
    this.setState(state => ({ pageNumber: state.pageNumber !== 1 ? state.pageNumber - 1 : state.pageNumber }));

  goToNextPage = () =>
    this.setState(state => ({ pageNumber: state.pageNumber !== this.state.numPages ? state.pageNumber + 1 : state.pageNumber }));

  openDocumentDetails = () => {
    this.setState({
      popUpEditDocument: true,
    });
  };

  openConfirmationBox = () => {
    this.setState({
      deleteDocument: true,
    });
  };

  _handleDeleteDocument = () => {
    this.props.deleteDocumentFlow(this.props.docId);
    this.closeValidateDeleteDoc();
  };

  closeValidateDeleteDoc = () => {
    this.setState({
      deleteDocument: false,
    });
  };

  closeEditDocument = () => {
    this.setState({
      popUpEditDocument: false,
    });
  };

  updateDoc = () => {
    /* if (this.props.document.access === undefined) { return null; }
    if (this.props.document.access.length !== 0) { */
    return (
      <div>
        <div className="custom-button-item">
          <CustomIconButton
            onClickElement={this.openDocumentDetails}
            iconClass="material-icons icon-trad"
            iconName="edit"
            label="Modifier ce document" />
        </div>
        <div className="hr-separator">
          <hr />
        </div>
        <div className="custom-button-item">
          <CustomIconButton
            onClickElement={this.openConfirmationBox}
            iconClass="material-icons icon-trad"
            iconName="delete_outline"
            label="Supprimer ce document" />
        </div>
      </div>
    );
    // }
  };

  translation = () => {
  /*  if (this.props.document.access === undefined) { return null; }

    if (this.props.document.access.length === 0) { return; } */
    if (this.props.document.idTranslation === null) {
      return (
        <CustomIconButton
          onClickElement={() => {
            this.props.createDocumentTranslationFlow({ id: this.props.document.id });
            NavigationFlux.navigate(`/traduction/create/${this.props.document.id}`, 'replace');
          }}
          iconClass="material-icons icon-trad"
          iconName="photo_size_select_small"
          label="Créer une traduction" />
      );
    } else {
      return (
        <CustomIconButton
          onClickElement={() => {
            NavigationFlux.navigate(`/traduction/create/${this.props.document.id}`, 'replace');
          }}
          iconClass="material-icons icon-trad"
          iconName="photo_size_select_small"
          label="Reprendre la traduction" />
      );
    }
  };

  displayHLS = () => {
    if (this.state.selectedVideo) {
      return (<DisplayHLS selectedVideo={this.state.selectedVideo}/>);
    }
  }

  doNothing(idVideo) {
    this.setState({ selectedVideo: idVideo });
  }

  render() {
    const { pageNumber, numPages } = this.state;
    if (this.props.document === null) { return (<div>No Document</div>); }
    return (
      <div className="pdf-root-container">
        <PopupWindow title={'Modifier les détails d\'un document'} close={this.closeEditDocument}
          isOpen={this.state.popUpEditDocument}>
          <EditDocumentDetails document={this.props.document} />
        </PopupWindow>
        <PopupWindow
          title={'Supprimer un document'}
          close={this.closeValidateDeleteDoc}
          isOpen={this.state.deleteDocument}>
          <Confirmation
            boxTitle={'Êtes-vous sûr de vouloir supprimer ce document ?'}
            deletionConfirmed={this._handleDeleteDocument}
            deletionCanceled={this.closeValidateDeleteDoc} />
        </PopupWindow>
        <Document
          className="document-pdf"
          file={this.props.document.link}
          onLoadSuccess={this.onDocumentLoadSuccess}>
          <Page width={610} className="page-pdf" pageNumber={pageNumber} />
          {this.props.docTraduction &&
             <DragBoxView
               onClickElement={this.doNothing}
               docTraduction={this.props.docTraduction.translation}
               pageNumber={pageNumber - 1} />}
        </Document>
        <div className="right-control-panel">
          <div className="button-helper-container">
            <div className="custom-button-item">
              {this.translation()}
            </div>
          </div>
          {this.updateDoc()}
          {this.displayHLS()}
        </div>
        <div className="controls-pdf-container">
          <button className="material-icons button-pdf-controls" onClick={this.goToPrevPage}>
            keyboard_arrow_left
          </button>
          <p className="page-number-label">Page {pageNumber} sur {numPages}</p>
          <button className="material-icons button-pdf-controls" onClick={this.goToNextPage}>
            keyboard_arrow_right
          </button>
        </div>

      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {};
};

const mapDispatchToPropsTransient = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToPropsTransient)(PdfView);
