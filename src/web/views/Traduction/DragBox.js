import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Rnd } from 'react-rnd';
import { bindActionCreators } from 'redux';
import { updateTranslation } from '../../../flux/Translations/Actions';


import { toPixel, toPercent} from '../../../flux/PixelToPercent';

class DragBox extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      itemSelected: null,
      zones: this.props.translation,
      style1: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        border: 'solid 1px black',
        zIndex: 99,
        background: 'rgba(0, 0, 0, 0.2)',
      },
      style2: {
        zIndex: 2,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        border: 'solid 1px black',
        background: 'rgba(0, 255, 0, 0.2)',
      },
      style3: {
        zIndex: 2,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        border: 'solid 1px black',
        background: 'rgba(0, 0, 255, 0.2)',
      },
    };
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextProps.deleteZone) { return true; }
    if (nextProps.isZoneAndBindConfirmed) { return true; }
    if (nextProps.selectedVideo !== null || nextProps.selectedVideo !== undefined) { return true; }
    if (nextProps.test !== this.props.test) { return true; }
    return false;
  }

  componentDidUpdate(prevProps) {
    if (this.state.zones[this.props.pageNumber] === undefined) {
      let zones = this.state.zones;
      zones[this.props.pageNumber] = [];
      this.setState({
        ...this.state,
        zones,
      });
    }
    if (this.props.test !== prevProps.test) { this.bindVideoToZone(); }
    if (this.props.deleteZone !== prevProps.deleteZone) { this.removeBox(); }
    if (this.props.creatingZone !== prevProps.creatingZone) { this.createZone(); }
  }

  bindVideoToZone() {
    let zones = this.state.zones;
    if (this.props.selectedVideo) {
      zones[this.props.pageNumber][this.state.itemSelected].idVideo = this.props.selectedVideo;
      this.setState({
        ...this.state,
        zones,
      });
      this.updateSelected();
    }
  };

  createZone() {
    let zones = this.state.zones;
    Array.from(zones[this.props.pageNumber].push({
      x: toPercent(610,0),
      y: toPercent(862,80),
      width: toPercent(610,200),
      height: toPercent(862,100),
      idVideo: null,
    }));
    this.setState({
      ...this.state,
      zones,
    });
    this.updateSelected();
  }

  removeBox() {
    let zones = this.state.zones;
    zones[this.props.pageNumber].splice(this.state.itemSelected, 1);
    this.setState({
      ...this.state,
      zones,
    }, () => {
      this.updateSelected();
    });
  }

  onDragStop(e, d, index) {
    let zones = this.state.zones;
    let accuratePosition = DragBox._getAccuratePosition(d.x, d.y, d.node.offsetWidth, d.node.offsetHeight, zones[this.props.pageNumber][index]);
    zones[this.props.pageNumber][index] = {
      ...accuratePosition,
    };
    this.setState({
      ...this.state,
      zones,
    }, () => {
      this.updateSelected();
    });
    e.preventDefault();
    e.stopPropagation();
  }

  static _getAccuratePosition(x, y, width, height, zones) {
    let accuratePosition = {
      ...zones,
      x: toPercent(610,x),
      y: toPercent(862,y),
      width: toPercent(610,width),
      height: toPercent(862,height),
    };
    return accuratePosition;
  }

  onResize(e, direction, ref, delta, position, index) {
    let zones = this.state.zones;
    zones[this.props.pageNumber][index].width = toPercent(610,parseFloat(ref.style.width, 10));
    zones[this.props.pageNumber][index].height = toPercent(862,parseFloat(ref.style.height, 10));
    zones[this.props.pageNumber][index].x = toPercent(610,parseFloat(position.x, 10));
    zones[this.props.pageNumber][index].y = toPercent(862,parseFloat(position.y, 10));
    this.setState({
      ...this.state,
      zones,
    });
    e.preventDefault();
    e.stopPropagation();
  }

  updateSelected() {
    this.props.updateTranslation({
      id: this.props.docTraduction.id,
      idDocument: this.props.idDocument,
      translation: this.state.zones,
    });
  }

  onMouseDown(e, id) {
 
    this.setState({
      ...this.state,
      itemSelected: id.index,
    });
    this.props.onClickTranslation(e, id);
    e.preventDefault();
    e.stopPropagation();
  }

  selectStyle(index) {
    if (this.props.focus !== undefined) {
      if (this.props.focus.index === index && this.props.focus.pageNumber === this.props.pageNumber) {
        return (this.state.style1);
      }
      if (!this.state.zones[this.props.pageNumber][index].idVideo ||
        this.state.zones[this.props.pageNumber][index].linkTrad === '') {
        return (this.state.style3);
      } else {
        return (this.state.style2);
      }
    }
    if (!this.state.zones[this.props.pageNumber][index].idVideo ||
      this.state.zones[this.props.pageNumber][index].linkTrad === '') {
      return (this.state.style3);
    }
    return (this.state.style2);
  }

  render() {
    return (
      this.state.zones[this.props.pageNumber] !== null &&
      this.state.zones[this.props.pageNumber] !== undefined &&
      Array.from(
        this.state.zones[this.props.pageNumber],
        (el, index) => (
          <Rnd
            onClick={(e) => {
              this.onMouseDown(e, {
                index,
                pageNumber: this.props.pageNumber,
                translation: this.state.zones[this.props.pageNumber][index],
              });
            }}
            bounds={'parent'}
            key={index}
            style={this.selectStyle(index)}
            size={{
              width: toPixel(610, parseFloat(this.state.zones[this.props.pageNumber][index].width, 10)),
              height: toPixel(862, parseFloat(this.state.zones[this.props.pageNumber][index].height, 10)),
            }}
            position={{
              x: toPixel(610, parseFloat(this.state.zones[this.props.pageNumber][index].x, 10)),
              y: toPixel(862, parseFloat(this.state.zones[this.props.pageNumber][index].y, 10)),
            }}
            onDragStop={(e, d) => {
              this.onDragStop(e, d, index);
            }}
            onResize={(e, direction, ref, delta, position) => {
              this.onResize(e, direction, ref, delta, position, index);
            }}
            onResizeStop={() => {
              this.updateSelected();
            }}
            default={{
              x: parseFloat(el.x, 10),
              y: parseFloat(el.y, 10),
              width: parseFloat(el.width, 10),
              height: parseFloat(el.height, 10),
            }}>
          </Rnd>
        )
      )
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {};
};

const mapDispatchToPropsTransient = (dispatch) => {
  return {
    updateTranslation: bindActionCreators(updateTranslation, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToPropsTransient)(DragBox);
