import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { deleteDocumentFlow, documentFlowById, getDocumentById } from '../../../flux/Documents/Actions';
import DocumentHelper from '../../../flux/Documents/Selector';
import { createDocumentTranslationFlow, getDocumentTranslation } from '../../../flux/Translations/Actions';
import TraductionHelper from '../../../flux/Translations/Selector';
import { deleteVideoTranslationById } from '../../../flux/TranslationVideo/Actions';
import VideoTranslationHelper from '../../../flux/TranslationVideo/Selector';
import SessionHelper from 'flux/Session/Selector';
import VideoHelper from '../../../flux/Videos/Selector';

import ReactPlayer from 'react-player';

import DashboardLeft from '../../components/DashboardLeft';
import PdfDisplay from './PdfDisplay';

import DragBoxView from './DragBoxView';




import ViewDocumentPanel from '../../components/ViewDocumentPanel';



class TraductionView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      deleteDocument: false,
      popUpEditDocument: false,
      numPages: null,
      pageNumber: 1,
      selectedZone: undefined

    };

  }

  componentDidMount() {
    this.props.documentFlowById({ id: this.props.match.params.id });

    // document.addEventListener('keydown', this.nextOrPrevPage, false);
  }

  nextOrPrevPage(e) {
    if (e.keyCode === 37) { this.goToPrevPage(); }
    if (e.keyCode === 39) { this.goToNextPage(); }
  }

  render() {
    return (
      <div className="page-container">
        <DashboardLeft />
        <div className="dashboard-middle">
          <div className={"traduction-toolbar"}>
            <ViewDocumentPanel
              docId={this.props.docId}
              deleteDocumentFlow={this.props.deleteDocumentFlow}
              createDocumentTranslationFlow={this.props.createDocumentTranslationFlow}
              document={this.props.document}
            />
          </div>
          {this.props.document &&
          <div className={"pdf-box"}>
            {
              this.props.document &&
              <PdfDisplay
                document={this.props.document}
                docTraduction={this.props.docTraduction}
                token={this.props.token}
              >
                <DragBoxView
                  onClickElement={this._doNothing}
                  docTraduction={this.props.docTraduction}
                />
              </PdfDisplay>
            }
            <div className="right-control-panel">
              {
                this.state.selectedZone ?
                <ReactPlayer url={this.state.selectedZone.assets.hls} controls /> :
                <div className={'player-empty'}>
                    <p>Cliquez sur une zone verte pour voir la traduction</p>
                </div>
              }
            </div>
          </div>
          }
          {this.props.videoTranslation &&
          <ReactPlayer url={this.props.videoTranslation.assets.hls} controls/>
          }
        </div>
      </div>

    );
  }

  _doNothing = (idVideo) => {
    this.setState({
      ...this.state,
      selectedZone: idVideo
    })
    return true;
  }

  _handleDeleteDocument() {
    this.props.deleteDocumentFlow(this.props.docId);
    this.closeValidateDeleteDoc();
  }

  _handleDeleteVideoTranslation() {
    this.props.deleteVideoTranslation({ id: this.props.videoTranslation.id });
  }
}

const mapStateToProps = (state, ownProps) => {
  const currentId = ownProps.match.params.id;
  return {
    token: SessionHelper.getHeaderAuthorization(),
    value: state.session,
    docId: currentId,
    document: DocumentHelper.getDocumentById(currentId),
    docTraduction: TraductionHelper.getTranslationForDoc(currentId),
    videoTranslation: VideoTranslationHelper.getVideoTranslationById({ id: currentId }),
    getPullDocumentStatus: DocumentHelper.getPullDocumentStatus(),
  };
};

const mapDispatchToPropsTransient = (dispatch) => {
  return {
    deleteVideoTranslation: bindActionCreators(deleteVideoTranslationById, dispatch),
    deleteDocumentFlow: bindActionCreators(deleteDocumentFlow, dispatch),
    createDocumentTranslationFlow: bindActionCreators(createDocumentTranslationFlow, dispatch),
    documentFlowById: bindActionCreators(documentFlowById, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToPropsTransient)(TraductionView);
