import React, { Component } from 'react';
import { Document, Page, pdfjs } from 'react-pdf';


pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;

export default class PdfDisplay extends Component {

  constructor(props) {
    super(props);
    this.state = {
      numPages: null,
      pageNumber: 1,
    };
  }

  onDocumentLoadSuccess = ({ numPages }) => {
    this.setState({ numPages, pageNumber: 1 });
  };

  goToPrevPage = () =>
    this.setState(state => ({ pageNumber: state.pageNumber !== 1 ? state.pageNumber - 1 : state.pageNumber }));

  goToNextPage = () =>
    this.setState(state => ({ pageNumber: state.pageNumber !== this.state.numPages ? state.pageNumber + 1 : state.pageNumber }));

  render() {
    const { pageNumber, numPages } = this.state;
    return (
      <div className={'pdf-box-content'}>
        <Document
          className="document-pdf"
          file={this.props.document.link + '?accessToken=' + this.props.token.split(' ')[1]}
          onLoadSuccess={this.onDocumentLoadSuccess}>
          <Page width={610} className="page-pdf" pageNumber={pageNumber}/>
          {
            this.props.docTraduction &&
            React.cloneElement(this.props.children, {
              pageNumber: pageNumber - 1,
            })
          }
        </Document>

        <div className="controls-pdf-container">
          <button className="material-icons button-pdf-controls" onClick={this.goToPrevPage}>keyboard_arrow_left
          </button>
          <p className="page-number-label">Page {pageNumber} sur {numPages}</p>
          <button className="material-icons button-pdf-controls" onClick={this.goToNextPage}>keyboard_arrow_right
          </button>
        </div>
      </div>
    );
  }
}
