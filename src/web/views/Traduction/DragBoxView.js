import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Rnd } from 'react-rnd';
import { bindActionCreators } from 'redux';
import { getVideoById } from '../../../flux/Videos/Actions';

import { toPixel } from '../../../flux/PixelToPercent';

class DragBoxView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      style1: {
        cursor: 'pointer',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        border: 'solid 1px black',
        background: 'rgba(255, 0, 0, 0.2)',
      },
      style2: {
        cursor: 'pointer',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        border: 'solid 1px black',
        background: 'rgba(0, 255, 0, 0.2)',
      },
      style3: {
        cursor: 'pointer',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        border: 'solid 1px black',
        background: 'rgba(0, 0, 255, 0.2)',
      },
    };
  }

  componentWillMount() {
    this.setState({
      ...this.state,
      zones: this.props.docTraduction.translation
    })
  }
  shouldComponentUpdate(nextProps, nextState) {
    if (this.state.zones !== nextProps.docTraduction.translation)
      return true;
    else
      return false;
  }
  componentDidUpdate() {
    this.setState({
      ...this.state,
      zones: this.props.docTraduction.translation
    })
  }

  selectStyle(index) {
    if (this.props.focus !== undefined) {
      if (this.props.focus.index === index && this.props.focus.pageNumber === this.props.pageNumber) {
        return (this.state.style1);
      }
      if (!this.state.zones[this.props.pageNumber][index].idVideo ||
        this.state.zones[this.props.pageNumber][index].linkTrad === '') {
        return (this.state.style3);
      } else {
        return (this.state.style2);
      }
    }
    if (!this.state.zones[this.props.pageNumber][index].idVideo ||
      this.state.zones[this.props.pageNumber][index].linkTrad === '') {
      return (this.state.style3);
    }
    return (this.state.style2);
  }

  render() {
    return (
      this.state.zones[this.props.pageNumber] !== null &&
      this.state.zones[this.props.pageNumber] !== undefined &&
      Array.from(
        this.state.zones[this.props.pageNumber],
        (el, index) => (
          <Rnd
            onClick={() => {
              this.props.onClickElement(this.state.zones[this.props.pageNumber][index].idVideo);
            }}
            bounds={'parent'}
            key={index}
            style={this.selectStyle(index)}
            size={{
              width: toPixel(610, parseFloat(this.state.zones[this.props.pageNumber][index].width, 10)),
              height: toPixel(862, parseFloat(this.state.zones[this.props.pageNumber][index].height, 10)),
            }}
            position={{
              x: toPixel(610, parseFloat(this.state.zones[this.props.pageNumber][index].x, 10)),
              y: toPixel(862, parseFloat(this.state.zones[this.props.pageNumber][index].y, 10)),
            }}>
          </Rnd>
        )
      )
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {};
};

const mapDispatchToPropsTransient = (dispatch) => {
  return {
    getVideoById: bindActionCreators(getVideoById, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToPropsTransient)(DragBoxView);
