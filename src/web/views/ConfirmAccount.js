import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { confirmAccount, resendConfirmToken } from '../../flux/User/Actions';
import UserHelper from '../../flux/User/Selector';

import NavigationFlux from '../../flux/navigation.web';

export class ConfirmAccount extends Component {

  constructor(props){
    super(props);
    this.state = {
      sendNewToken : false
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    console.log('Store.status=>', this.props.getRegisterStatus.status);
    console.log('next.status=>', nextProps.getRegisterStatus.status);
    if (this.props.getRegisterStatus.status !== nextProps.getRegisterStatus.status) { return true; }
    return false;
  }
  componentDidMount() {
    this.props.confirmAccount({ token: this.props.match.params.token });

    // console.log("TOKEN====+>",this.props.match.params.token)
  }
  
  render() {
    return (
      <div className="page-container">

        <div className="dashboard-middle">

          {
            this.props.getRegisterStatus.status === 'Loading'
              ? <div className={' account-verif-message'}><p className="account-verif-message-item">Veuillez patienter pendant que nous vérifions votre compte..</p></div>
              : this.props.getRegisterStatus.status === 'Failure'
                ? <div className={' account-verif-message'}><p className="account-verif-message-item">Oups il semblerait que nous ayons des problèmes pour vérifier votre compte</p>
                  <p className="account-verif-message-item">{
                    this.props.getRegisterStatus.error.message === 'User not found or token expired' ? 'La demande a expiré' : 'Accès interdit'
                  }</p>
                  <p  onClick={() =>{
                     this.props.resendConfirmToken();
                      this.setState({...this.state,sendNewToken : true})
                     }} className={' account-verif-message-item account-verif-message-item-link'}>Envoyer une nouvelle demande.</p>
                  <p className={' account-verif-message-item account-verif-message-item'}>Si le problème persiste, contactez le support.</p>
                </div>
                : <div className={' account-verif-message'}><p className="account-verif-message-item">
                                   {this.state.sendNewToken === true ?
                                    "Nous vous avons envoyé un nouvel e-mail, consultez votre boîte mail." :
                                    "Nous avons vérifié votre compte avec succès, vous pouvez maintenant utiliser pleinement tous nos services."
                                   } 
                </p>
                <p onClick={() => {
                  NavigationFlux.navigate(`/`, 'replace');
                }} className={' account-verif-message-item account-verif-message-item-link'}>Accueil</p>

                </div>}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    getRegisterStatus: UserHelper.getRegisterStatus(),
  };
};

const mapDispatchToPropsTransient = (dispatch) => {
  return {
    confirmAccount: bindActionCreators(confirmAccount, dispatch),
    resendConfirmToken: bindActionCreators(resendConfirmToken, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToPropsTransient)(ConfirmAccount);
