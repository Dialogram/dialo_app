import React, { Component } from 'react';
import '../../css/auth/register.css';
import Footer from '../components/Footer';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { createUser, resetUserError } from '../../flux/User/Actions';
import SessionHelper from '../../flux/Session/Selector';
import Utils from '../helpers/Utils';
import FormErrors from '../components/FormErrors';
import NavigationFlux from '../../flux/navigation.web';
import UserHelper from '../../flux/User/Selector';

class Register extends Component {
  constructor(props) {
    super(props);

    this.state = {
      CGUs: false,
      CGUsError: '',
      firstName: '',
      lastName: '',
      nickName: '',
      email: '',
      password: '',
      password2: '',
      formErrors: { firstName: '', lastName: '', nickName: '', email: '', password: '', password2: '' },
      firstNameValid: false,
      lastNameValid: false,
      nickNameValid: false,
      emailValid: false,
      passwordValid: false,
      password2Valid: false,
      formValid: false,
      disabled: true,
      displayError: false
    };

    this._CGUs = this._CGUs.bind(this);
    this._handleInputChange = this._handleInputChange.bind(this);
    this._signUp = this._signUp.bind(this);
    this._validateField = this._validateField.bind(this);
    this._onBlur = this._onBlur.bind(this);
    this._handleEnterKeyPress = this._handleEnterKeyPress.bind(this);
  }

  onClickAuthLink() {
    NavigationFlux.navigate('auth', 'replace');
  }
  componentWillUnmount(){
    console.log("leave register");
    this.props.resetUserError();
  }
  render() {
    return (
      <div className="root-register-container">
        <div className="register-form-container">
          <div className="register-header-container">
            <img
              alt={'logo'}
              className={'register-header-logo'}
              src={require('image/2020_logo_DIALOGRAM.png')}
            />
            <h1 className="register-title">Dialogram</h1>
          </div>
          <div className="register-container">
            <form className="register-form">
              <div className="register-input-container">

                <input className="register-input"
                  value={this.state.firstName}
                  name="firstName"
                  id={'firstName'}
                  type={'text'}
                  placeholder={'Prénom'}
                  onKeyPress={this._handleEnterKeyPress}
                  onBlur={this._onBlur}
                  onChange={this._handleInputChange}/>
                <FormErrors fieldName='firstName' message={this.state.formErrors.firstName} />

                <input className="register-input"
                  value={this.state.lastName}
                  name="lastName"
                  id={'lastName'}
                  type={'text'}
                  placeholder={'Nom'}
                  onKeyPress={this._handleEnterKeyPress}
                  onBlur={this._onBlur}
                  onChange={this._handleInputChange}/>
                <FormErrors fieldName='lastName' message={this.state.formErrors.lastName} />

                <input className="register-input"
                  value={this.state.nickName}
                  name="nickName"
                  id={'nickName'}
                  type={'text'}
                  placeholder={'Identifiant'}
                  onKeyPress={this._handleEnterKeyPress}
                  onBlur={this._onBlur}
                  onChange={this._handleInputChange}/>
                <FormErrors fieldName='nickname' message={this.state.formErrors.nickName} />

                <input className="register-input"
                  placeholder="Email"
                  value={this.state.email}
                  name="email"
                  id={'email'}
                  type={'email'}
                  onKeyPress={this._handleEnterKeyPress}
                  onBlur={this._onBlur}
                  onChange={this._handleInputChange}/>
                <FormErrors fieldName='email' message={this.state.formErrors.email} />

                <input className="register-input"
                  placeholder="Mot de passe"
                  value={this.state.password}
                  name="password"
                  id={'password'}
                  type={'password'}
                  onKeyPress={this._handleEnterKeyPress}
                  onBlur={this._onBlur}
                  onChange={this._handleInputChange}/>
                <FormErrors fieldName='password' message={this.state.formErrors.password} />

                <input className="register-input"
                  placeholder="Confirmation de mot de passe"
                  value={this.state.password2}
                  name="password2"
                  id={'password2'}
                  type={'password'}
                  onKeyPress={this._handleEnterKeyPress}
                  onBlur={this._onBlur}
                  onChange={this._handleInputChange}
                />
                <FormErrors fieldName='password2' message={this.state.formErrors.password2} />
                <div className="checkbox-root-container">
                  <input
                    name="CGUs"
                    type="checkbox"
                    onChange={this._CGUs} />
                  <label>
                      J&apos;ai lu et j&apos;accepte les <a className="cgu-label" href="https://dialogram.fr/cgu.html">Conditions Générales d&apos;Utilisation</a>
                  </label>

                </div>
                <FormErrors fieldName='CGUs' message={this.state.CGUsError} />
                {this.props.userError.toJS().message}
                <button className="register-button" onClick={this._signUp} disabled={this.state.disabled}>S&apos;inscrire</button>

              </div>
            </form>
          </div>
        </div>
        <div className="auth-link-container">
          <a onClick={this.onClickAuthLink} className="auth-link">J&apos;ai déjà un compte</a>
        </div>
        <div className="get-app-container">
          <p className="get-app-p">Téléchargez l&apos;application</p>
          <div className="get-app-images">
            <img
              alt={'GooglePlay'}
              className="get-it-on-google-play"
              src={require('image/get-it-on-google-play.png')}
            />
          </div>
        </div>
        <div className="footer-component">
          <Footer/>
        </div>
      </div>
    );
  }

  _CGUs() {
    this.setState({ CGUs: !this.state.CGUs });
  }

  _handleEnterKeyPress(e) {
    if (e.key === 'Enter') {
      this._onBlur(e);
      if (this.state.disabled === false) {
        this._signUp(e);
      }
    }
  }

  _onBlur(e) {
    this._validateField(e.target.name, e.target.value);
  }

  _validateField(name, value) {
    let fieldValidationErrors = this.state.formErrors;
    let firstNameValid = this.state.firstNameValid;
    let lastNameValid = this.state.lastNameValid;
    let nickNameValid = this.state.nickNameValid;
    let emailValid = this.state.emailValid;
    let passwordValid = this.state.passwordValid;
    let password2Valid = this.state.password2Valid;

    switch (name) {
      case 'firstName':
        if (value.length >= 3 && value.length <= 22) {
          firstNameValid = value.match(Utils.NO_NUMBER_REGEX);
          fieldValidationErrors.firstName = firstNameValid ? '' : Utils.ERRORS['NoNumber'];
        } else if (value.length > 22) {
          firstNameValid = false;
          fieldValidationErrors.firstName = Utils.ERRORS['MaxLen22'];
        } else {
          firstNameValid = false;
          fieldValidationErrors.firstName = Utils.ERRORS['NameLen3'];
        }
        break;
      case 'lastName':
        if (value.length >= 3 && value.length <= 22) {
          lastNameValid = value.match(Utils.NO_NUMBER_REGEX);
          fieldValidationErrors.lastName = lastNameValid ? '' : Utils.ERRORS['NoNumber'];
        } else if (value.length > 22) {
          lastNameValid = false;
          fieldValidationErrors.lastName = Utils.ERRORS['MaxLen22'];
        } else {
          lastNameValid = false;
          fieldValidationErrors.lastName = Utils.ERRORS['NameLen3'];
        }
        break;
      case 'nickName':
        if (value.length >= 5 && value.length <= 15) {
          nickNameValid = true;
          fieldValidationErrors.nickName = '';
        } else if (value.length > 15) {
          nickNameValid = false;
          fieldValidationErrors.nickName = Utils.ERRORS['MaxLen15'];
        } else {
          nickNameValid = false;
          fieldValidationErrors.nickName = Utils.ERRORS['NickLen'];
        }
        break;
      case 'email':
        emailValid = value.match(Utils.EMAIL_REGEX);
        fieldValidationErrors.email = emailValid ? '' : Utils.ERRORS['WrongEmail'];
        break;
      case 'password':
        passwordValid = value.length >= Utils.EMAIL_MAX_LENGTH;
        fieldValidationErrors.password = passwordValid ? '' : Utils.ERRORS['PassLen'];
        break;
      case 'password2':
        password2Valid = value === this.state.password;
        fieldValidationErrors.password2 = password2Valid ? '' : Utils.ERRORS['SamePass'];
        break;
      default:
        break;
    }
    this.setState({
      formErrors: fieldValidationErrors,
      firstNameValid: firstNameValid,
      lastNameValid: firstNameValid,
      nickNameValid: nickNameValid,
      emailValid: emailValid,
      passwordValid: passwordValid,
      password2Valid: password2Valid,
    }, this.validateForm);
  };

  validateForm() {
    this.setState({ disabled: !(this.state.firstNameValid &&
                this.state.lastNameValid &&
                this.state.nickNameValid &&
                this.state.emailValid &&
                this.state.passwordValid &&
                this.state.password2Valid) });
  };

  _handleInputChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  };

  _signUp(e) {
    if (this.state.CGUs === false) {
      this.setState({ CGUsError: 'Vous devez accepter nos CGUs' });
      return;
    } else {
      this.setState({ CGUsError: '' });
    }
    const { from } = { from: { pathname: '/' } };
    this.props.createUser({
      nickName: this.state.nickName,
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      email: this.state.email,
      password: this.state.password,
      from,
    });
    e.preventDefault();
  };
}

const mapStateToProps = (state, ownProps) => {
  return {
    sessionError: SessionHelper.getSessionError(),
    userError: UserHelper.getUserError()
  };
};

const mapDispatchToPropsTransient = (dispatch) => {
  return {
    createUser: bindActionCreators(createUser, dispatch),
    resetUserError: bindActionCreators(resetUserError, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToPropsTransient)(Register);
