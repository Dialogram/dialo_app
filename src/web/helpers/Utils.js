export default class Utils {
  static NO_NUMBER_REGEX = /^([^0-9]*)$/;
  static EMAIL_REGEX = /^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i;
  static EMAIL_MAX_LENGTH = 8;

  static ERRORS = {
    'NoNumber': 'Ce champ ne peut contenir de numéro',
    'NickLen': 'Veuillez entrez un identifiant d\'au moins 5 caractères.',
    'NameLen3': 'Veuillez entrez un nom d\'au moins 3 caractères.',
    'NameLen': 'Entrez un nom d\'au moins 5 caractères.',
    'WrongEmail': 'Veuillez entrer un email valide.',
    'PassLen': 'Veuillez entrer un mot de passe d\'au moins 8 caractères.',
    'SamePass': 'Veuillez entrer un mot de passe identique.',
    'Empty': 'Ce champ est obligatoire.',
    'MissFile': 'Veuillez choisir un fichier.',
    'MissCategory': 'Veuillez choisir une catégorie.',
    'MissType': 'Veuillez choisir un type de fichier.',
    'MaxLen22': 'Vous avez dépassé la limite de 22 caractères.',
    'MaxLen15': 'Vous avez dépassé la limite de 15 caractères.',
  };
}
