import React from 'react';
import Utils from './Utils';
export const SUBMIT = 'Enregistrer';

export const ERRORS = {
  'MinLength': ' caractères minimum.',
  'MaxLength': ' caractères maximum',
  'Required': 'Champ requis',
  'EMAIL_VALID': "L'email doit être valide."
};


export const renderField = ({
  input,
  label,
  placeholder,
  type,
  meta: { touched, error, warning },
}) => (
    <div className="fieldSettings">
      <legend className="labelSettings">{label}</legend>
      <input className="inputSettings" {...input} placeholder={placeholder} type={type} />
      {touched &&
        ((error && <span>{error}</span>) ||
          (warning && <span>{warning}</span>))}
    </div>
  );

export const genderField = ({
  input,
  label,
  placeholder,
  type,
  meta: { touched, error, warning },
}) => (
    <div className="fieldSettings">
      <legend className="genderSettings">{label}</legend>
      <input className="inputSettings" {...input} placeholder={placeholder} type={type} />
      {touched &&
        ((error && <span>{error}</span>) ||
          (warning && <span>{warning}</span>))}
    </div>
  );

export const required = value => (value || typeof value === 'number' ? undefined : `${ERRORS.Required}`);

const maxLength = max => value =>
  value && value.length > max ? `${max} ${ERRORS.MaxLength}` : undefined;
const minLength = min => value =>
  value && value.length < min ? `${min} ${ERRORS.MinLength}` : undefined;
export const minLength3 = minLength(3);
export const minLength5 = minLength(5);
export const maxLength22 = maxLength(22);

export const email = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? 'Invalid email address'
    : undefined
