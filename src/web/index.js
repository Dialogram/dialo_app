import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';

import './config';
import 'css/index.css';
import store from 'flux/redux';

import { Provider } from 'react-redux';

import Wrapper from './Wrapper';

class App extends Component {
  render() {
    return (
      <Provider store={store}>

        <Wrapper />

      </Provider>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
