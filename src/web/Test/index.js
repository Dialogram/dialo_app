import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { createSession } from 'flux/Session/Actions';
import { getAllVideo } from 'flux/Videos/Actions';
import SessionHelper from 'flux/Session/Selector';
import Videohelper from 'flux/Videos/Selector';
import UserHelper from 'flux/User/Selector';

import { createUser } from 'flux/User/Actions';
import { getAllDocument, getDocumentById, documentPdfUpdate, removeDocuments } from 'flux/Documents/Actions';
import DocumentHelper from 'flux/Documents/Selector';

import { Logout } from 'flux/baseAction';

import ReactJson from 'react-json-view';
export class Test extends Component {
  render() {
    console.log('document list', this.props.allDocument);
    return (<div>

      <ReactJson name="Session" src={{
        status: this.props.sessionStatus,
        Token: this.props.sessionToken,
        Id: this.props.sessionId,
        UserId: this.props.sessionUserId,
        Error: this.props.sessionError.message,
      }} theme={'monokai'} />

      <button onClick={() => { this.props.createSession({ email: 'a@a.frs', password: 'zouzou100', from: '/' }); }}>CreateSessionError</button>
      <button onClick={() => { this.props.createSession({ email: 'a@a.fr', password: 'zouzou100', from: '/' }); }}>CreateSessionSuccess</button>
      <button onClick={() => { this.props.logout(); }}>Logout</button>
      <br /><br />

      <ReactJson name="User" src={{
        UserId: this.props.UserId,
        UserStatus: this.props.UserStatus,
        UserNickname: this.props.UserNickname,
        UserProfile: this.props.UserProfile,
        UserEmail: this.props.UserEmail,
      }} theme={'monokai'} />

      <button onClick={() => { this.props.createUser({ nickName: 'zfkvdgfpvzefbde', firstName: 'effhef', lastName: 'zihpjd', email: 'r@d.fr', password: 'ziboulou', from: '/' }); }}> createUser </button>
      <br /><br />

      <ReactJson name="Videos" src={{
        Status: this.props.videoStatus,
        Error: this.props.videoError,
        Videos: this.props.videoList || {},
      }} theme={'monokai'} />
      <button onClick={() => { this.props.getAllVideo(); }}> getAllVideo </button>
      <button onClick={() => { this.props.removeVideos(); }}> Remove Videos</button>
      <br /> <br/>
      <ReactJson name="Documents" src={{
        Status: this.props.documentStatus,
        Error: this.props.documentError,
        Documents: this.props.allDocument || {},
      }} theme={'monokai'} />
      <button onClick={() => { this.props.getAllDocument(); }}>Pull Documents </button>
      <button onClick={() => { this.props.getDocumentById('5c2efc4a251b9405a9539795'); }}>Pull Documents By Id </button>
      <button onClick={() => { this.props.documentPdfUpdate({ id: '5c2efc4a251b9405a9539795' }, { name: 'toto', description: 'desc', publicDoc: true }); }}>Pdf Update By Id</button>
      <button onClick={() => { this.props.removeDocuments(); }}> Remove Doc</button>
    </div>);
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    sessionStatus: SessionHelper.getSessionStatus(),
    sessionToken: SessionHelper.getSessionToken(),
    sessionId: SessionHelper.getSessionId(),
    sessionUserId: SessionHelper.getSessionUserId(),
    sessionError: SessionHelper.getSessionError(),
    hasSessionActive: SessionHelper.hasSessionActive(),
    videoList: Videohelper.getVideoList(),
    videoStatus: Videohelper.getVideoStatus(),
    videoError: Videohelper.getVideoError(),
    getHeaderAuthorization: SessionHelper.getHeaderAuthorization(),

    UserId: UserHelper.getUserId(),
    UserStatus: UserHelper.getUserStatus(),
    userNickname: UserHelper.getUserNickname(),
    userProfile: UserHelper.getUserProfile(),
    userEmail: UserHelper.getUserEmail(),

    document: DocumentHelper.getDocumentById('5c2efc4a251b9405a9539795'),
    allDocument: DocumentHelper.getAllDocument(),
    documentStatus: DocumentHelper.getDocumentStatus(),
    documentError: DocumentHelper.getDocumentError(),
  };
};

const mapDispatchToPropsTransient = (dispatch) => {
  return {
    createSession: bindActionCreators(createSession, dispatch),
    getAllVideo: bindActionCreators(getAllVideo, dispatch),
    createUser: bindActionCreators(createUser, dispatch),
    logout: bindActionCreators(Logout, dispatch),
    getAllDocument: bindActionCreators(getAllDocument, dispatch),
    getDocumentById: bindActionCreators(getDocumentById, dispatch),
    documentPdfUpdate: bindActionCreators(documentPdfUpdate, dispatch),
    removeDocuments: bindActionCreators(removeDocuments, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToPropsTransient)(Test);
