import React, { Component } from 'react';

export default class ActiveUserWindow extends Component {
  render() {

    return (
      <div className="popup-window" >
        <div className="content" onClick={(evt) => { evt.stopPropagation(); }}>
          <div className="popup-tool-bar">
          
          </div>
          <div className={'popup-children-box'}>
            Activez votre compte.
            <a onClick={this.props.resendConfirmToken}> Renvoyer une demande par e-mail</a>
          </div>
        </div>
      </div>
    );
  }
}
