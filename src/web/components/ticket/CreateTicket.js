import React, { Component } from 'react';
import CreateTicketForm from './CreateTicketForm';
export default class CreateTicket extends Component {
    constructor(props) {
        super(props)
        this.state = {
            displayTicket: false
        }
    }
    render() {
        return (
            <div className="ticket-container">
               
                    {
                        this.state.displayTicket ? <div>display</div> : <CreateTicketForm />
                    }
            </div>
        )
    }
}
/*                   
 <div className={"ticket-header"}>
                    <button className="ticket-header-item" onClick={(e) =>{
                        this.setState({
                            ...this.state,
                            displayTicket: false
                        })
                    }}>Créer un ticket</button>

                </div>
<button className="ticket-header-item"onClick={(e) =>{
                        this.setState({
                            ...this.state,
                            displayTicket: true
                        })
                    }}>Afficher les tickets</button>*/