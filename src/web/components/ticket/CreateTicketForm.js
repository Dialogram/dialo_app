import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import SessionHelper from '../../../flux/Session/Selector';
import axios from 'axios';


export class CreateTicketForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: "",
            description: "",
            categorie: "",
            formErrors: { title: '', description: '' },
            titleValid: false,
            descriptionValid: false,
            status: null
        }
    }

    onChange = (e) => {
        const { value, name } = e.target;
        this.setState({
            ...this.state,
            [name]: value
        })
    }
    onSubmit = (e) => {
        let titleError = false;
        let descError = false;
        if (this.state.title.length < 5) {

            titleError = true
        }
        if (this.state.description.length < 100) {
            descError = true
        }
        if (titleError || descError) {
            this.setState({
                ...this.state,
                formErrors: {
                    title: titleError ? "Veuillez renseigner un titre de plus de 5 caractères" : "",
                    description: descError ? "La description doit faire plus de 100 caractères." : ""
                }
            });
        } else {
            axios({
                method: 'post',
                url: 'http://localhost:3000/api/report',
                data: {
                    "title": this.state.title || "Bug on Video API",
                    "category": this.state.categorie || "bug",
                    "description": this.state.desc || "empty doit etre suppérieur a 100 caractère zfjazpojfzapôfjazp^fjhpazjhpoâpôfj^jâjfâz^jfâpjfâzjfazpjf"
                },
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'X-Requested-With': 'XMLHttpRequest',
                    'Authorization': this.props.sessionActive
                }
            }).then(() => {
                this.setState({
                    ...this.state,
                    status: "success",
                })
            }).catch(err => {
                this.setState({
                    ...this.state,
                    status: "failure",
                })
            });
        }
        e.preventDefault();

    }
    render() {
        return (
            <div >
                <form className="create-ticket-form-container">
                    <input className="create-ticket-form-item" type="text" name="title" onChange={this.onChange} value={this.state.title} />
                    <textarea className="create-ticket-form-item" type="text" name="description" onChange={this.onChange} value={this.state.description} />
                    <select className="create-ticket-form-item" id="ticket-select" name="categorie" onChange={this.onChange} value={this.state.categorie}>
                        <option value="bug">Bug</option>
                        <option value="user">Utilisateur</option>
                        <option value="content">Contenu</option>
                        <option value="feature">Fonctionnalité</option>
                        <option value="improvment">Amélioration</option>

                    </select>
                    <p>  {this.state.formErrors.title}</p>
                    <p>  {this.state.formErrors.description}</p>
                    {this.state.status && <p className={"ticket-success"}>Votre ticket a bien été envoyé.</p>}
                    <button onClick={this.onSubmit}>Envoyer</button>
                </form>
            </div>
        )
    }
}
const mapStateToProps = (state, ownProps) => {
    return {
        sessionActive: SessionHelper.getHeaderAuthorization(),
    };
};

const mapDispatchToPropsTransient = (dispatch) => {
    return {

    };
};

export default connect(mapStateToProps, mapDispatchToPropsTransient)(CreateTicketForm);