import React, { Component } from 'react';
import { connect } from 'react-redux';
import { CustomIconButton } from './common/CustomIconButton';

class RemoveBox extends Component {
  constructor(props) {
    super(props);

    this.state = {
      removeBox: false,
    };
  }

  render() {
    return (
      <div className="addfile-component-container">
        <h1>Êtes-vous sur de vouloir supprimer cette zone de traduction ?</h1>
        <div className="custom-button-item">
          <CustomIconButton
            onClickElement={this._doNothing}
            iconClass="material-icons icon-trad"
            iconName="delete_outline"
            label="Supprimer"/>
        </div>
      </div>
    );
  }

  _doNothing() {
    return this.setState({ removeBox: true });
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
  };
};

const mapDispatchToPropsTransient = (dispatch) => {
  return {

  };
};

export default connect(mapStateToProps, mapDispatchToPropsTransient)(RemoveBox);
