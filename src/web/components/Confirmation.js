import React, { Component } from 'react';
import { connect } from 'react-redux';
import { CustomIconButton } from './common/CustomIconButton';

class Confirmation extends Component {
  render() {
    return (
      <div className="addfile-component-container">
        <div className="label-container">
          <h3 className="label-title">{this.props.boxTitle}</h3>
        </div>
        <div className="two-button-choice">
          <div className="custom-button-item">
            <CustomIconButton
              onClickElement={this.props.deletionConfirmed}
              iconClass="material-icons icon-trad"
              iconName="done"
              label="Confirmer"/>
          </div>
          <div className="custom-button-item">
            <CustomIconButton
              onClickElement={this.props.deletionCanceled}
              iconClass="material-icons icon-trad"
              iconName="cancel_outline"
              label="Annuler"/>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
  };
};

const mapDispatchToPropsTransient = (dispatch) => {
  return {
  };
};

export default connect(mapStateToProps, mapDispatchToPropsTransient)(Confirmation);
