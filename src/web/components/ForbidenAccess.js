import React, { Component } from 'react';
import NavigationFlux from '../../flux/navigation.web';
export default class ForbidenAccess extends Component {
    render() {
        return (
            <div className="forbiden-content">
                <div><h3>Accès interdit</h3></div>
                <div>
                    <button onClick={
                        () => {
                            NavigationFlux.navigate(`/`, 'replace');
                        }
                    }>Retour</button>
                </div>
            </div>
        )
    }
}