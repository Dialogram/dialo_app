import React, { Component } from 'react';
import '../../css/Component/Component.css';
import '../../css/index.css';

class Footer extends Component {
  render() {
    return (
      <div className="footer-container">
        <span className="dialogram-rights">© 2019 Dialogram</span>
      </div>
    );
  }
}

export default Footer;
