import React, { Component, Fragment } from 'react';
import Comment from './Comment';

export default class CommentBox extends Component {
    createComment = () => {
      let elem = [];
      let len = this.props.comments.length;

      for (let a in this.props.comments) {
        if (a > (len - 6)) { elem.push(<Comment key={a} comment={this.props.comments[a]} />); }
      }
      return elem;
    }

    render() {
      return (
        <Fragment>
          {this.createComment()}
        </Fragment>
      );
    }
}
