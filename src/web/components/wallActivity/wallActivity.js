import React, { Component } from 'react';
import { Document, Page } from 'react-pdf/dist/entry.webpack';
import CommentBox from './CommentBox';
import { commentDocument, exploreDocument } from '../../../flux/Documents/Actions';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import DocumentHelper from '../../../flux/Documents/Selector';

export class WallActivity extends Component {
  constructor(props) {
    super(props);
    this.state = {
      comment: '',
      config: {
        category: 'health',
        field: 'dateCreation',
        sort: 'asc',
        limit: '10',
        page: '1',
      },
      loading: 'Default',
    };
  }

    onChange = (e) => {
      const { name, value } = e.target;
      this.setState({ [name]: value });
    }

    static getDerivedStateFromProps(props, state) {
      if (props.getPullDocumentStatus.status === 'Success' && state.loading !== props.getPullDocumentStatus.status) {
        props.exploreDocument(state.config);
        return { ...state, loading: 'Success' };
      }
      return state;
    }
    onSubmit = (e, idDocument) => {
      this.props.commentDocument({ id: idDocument }, { comment: this.state.comment });
      this.setState({
        ...this.state,
        loading: 'Default',
      });
    }

    componentDidMount() {
      this.props.exploreDocument(this.state.config);
    }

    createDocList = () => {
      if (this.props.documents === null) { return null; }
      let element = [];
      for (let e in this.props.documents) {
        element.push(
          <div className={'wall-item'} key={e}>
            <Document
              className={'wall-document-pdf'}
              file={this.props.documents[e].link}
            >
              <Page width={300} className="page-pdf" pageNumber={1} />
            </Document>
            <div className="wall-document-comment">
              <CommentBox comments={this.props.documents[e].features.comments} />
              <div>
                <textarea onChange={this.onChange} value={this.state.comment} className={'post-comment'} name="comment" cols="80" rows="1"></textarea>
                <button onClick={(ev) => {
                  console.log('clicked');
                  this.onSubmit(ev, e);
                }}>Commenter</button>
              </div>
            </div>
          </div>
        );
      }
      return element;
    }
    render() {
      if (this.props.documents === null) { return null; }
      return (
        <div className={'explorer-container wall-activity'}>

          {this.createDocList()}

        </div>
      );
    }
}

const mapStateToProps = (state, ownProps) => {
  return {
    documents: DocumentHelper.getExploreDocument(),
    getPullDocumentStatus: DocumentHelper.getDocumentFeatureStatus(),
  };
};

const mapDispatchToPropsTransient = (dispatch) => {
  return {
    commentDocument: bindActionCreators(commentDocument, dispatch),
    exploreDocument: bindActionCreators(exploreDocument, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToPropsTransient)(WallActivity);
