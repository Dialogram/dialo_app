import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { searchUser } from '../../../flux/Search/Actions';

export class Comment extends Component {
  render() {
    console.log(this.props.comment.ownerId.profile.profilePicture);
    return (
      <div className="wall-comment">
        <div className="wall-comment-wrapper">
          <div className="wall-comment-avatar">{this.props.comment.ownerId.profile.firstName} {this.props.comment.ownerId.profile.lastName}</div> <p className="wall-comment-content">:  {this.props.comment.comment}</p>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {

  };
};

const mapDispatchToPropsTransient = (dispatch) => {
  return {
    searchUser: bindActionCreators(searchUser, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToPropsTransient)(Comment);
