import React, { Component } from 'react';
import '../../css/Component/Component.css';
import '../../css/index.css';

export default class PopUpWindow extends Component {
  render() {
    const { close, isOpen } = this.props;

    if (close === undefined || isOpen === undefined) {
      return null;
    }
    if (isOpen === false) { return null; }

    return (
      <div className="popup-window" onClick={close}>
        <div className="content" onClick={(evt) => { evt.stopPropagation(); }}>
          <div className="popup-tool-bar">
            <i className="material-icons close" onClick={close}>close</i>
          </div>
          <div className={'popup-children-box'}>
            {this.props.children}
          </div>
        </div>
      </div>
    );
  }
}
