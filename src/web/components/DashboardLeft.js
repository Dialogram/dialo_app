import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { getAllDocument } from '../../flux/Documents/Actions';
import DocumentHelper from '../../flux/Documents/Selector';
import NavigationFlux from '../../flux/navigation.web';
import { getAllVideoTranslation } from '../../flux/TranslationVideo/Actions';
import VideoTranslationHelper from '../../flux/TranslationVideo/Selector';
// import VideoHelper from '../../flux/Videos/Selector';
import AddFile from '../components/AddFile';
import { DropDown } from '../components/DropDown';
import UserHelper from '../../flux/User/Selector';

import PopupWindow from '../components/PopupWindow';

export class DashboardLeft extends Component {
  constructor(props) {
    super(props);
    this.state = {
      popUpAddFile: false,

    };
  }
  componentDidMount() {
    this.props.getAllDocument({ limit: 5000 });
    this.props.getAllVideoTranslation();
  }
  onClickDropDownElement = (idDocumnt) => {
    NavigationFlux.navigate(`/traduction/view/${idDocumnt}`, 'replace');
  }

  openAddPdf = (e) => {
    this.setState({
      ...this.state,
      popUpAddFile: true,
    });
    e.preventDefault();
  }

  closeAddPdf = (e) => {
    this.setState({
      ...this.state,
      popUpAddFile: false,
    });
    e.preventDefault();
  }
  render() {
    return (
      <div className="dashboard-left">
        <PopupWindow title={'Ajouter un fichier'} close={this.closeAddPdf} isOpen={this.state.popUpAddFile}>
          <AddFile /></PopupWindow>

        <div className="profile-card-container">
          <div className="profile-picture-circle">
            <img
              alt={'profile'}
              className={'profile-image'}
              src={this.props.profile ? this.props.profile.profilePicture.url : null}
            />
          </div>
          <div className="profile-name-container">
            <p>{this.props.nickName}</p>
            <p>{this.props.userEmail}</p>
          </div>
        </div>
        <div className="hr-separator">
          <hr />
        </div>
        <div className={'profile-stat-container'}>
          <DropDown title="Documents" onClickElement={this.onClickDropDownElement}
            dataToShow={this.props.documentList} />
          {/* <DropDown title="Vidéos" onClickElement={Home.onClickDropDownElement} */}
          {/* dataToShow={this.props.videoList}/> */}
          <DropDown title="Vidéos de traductions" onClickElement={this.onClickDropDownElement}
            dataToShow={this.props.getVideoTranslationList} />
        </div>
        <div className="hr-separator">
          <hr />
        </div>
        <div className="upload-root-container">
          <div className={'profile-upload-container'} onClick={this.openAddPdf}>
            <img
              alt={'upload'}
              className={'upload-icon'}
              src={require('image/upload-icon.png')}
            />
            <div className="upload-file-container">
              Ajouter un fichier
              </div>

          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    documentList: DocumentHelper.getMyDocuments(),
    documentsStatus: DocumentHelper.getDocumentStatus(state),
    getVideoTranslationList: VideoTranslationHelper.getVideoTranslationList(state),
    profile: UserHelper.getUserProfile(state),
    nickName: UserHelper.getUserNickname(state),
    userEmail: UserHelper.getUserEmail(state),
  };
};

const mapDispatchToPropsTransient = (dispatch) => {
  return {
    getAllDocument: bindActionCreators(getAllDocument, dispatch),
    getAllVideoTranslation: bindActionCreators(getAllVideoTranslation, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToPropsTransient)(DashboardLeft);
