import React from 'react';
import '../../../css/Component/Component.css';
import '../../../css/index.css';

export class ItemList extends React.Component {
  render() {
    const { dataToShow } = this.props;

    return (
      <div className="list-container">
        {
          (dataToShow !== undefined && dataToShow !== null) ? Object.keys(dataToShow).map((key) => {
            if (dataToShow[key].id) {
              return (
                <div className={'dropdown-item'} key={key} onClick={() => {
                  this.props.onClickElement(dataToShow[key].id);
                }}>
                  {dataToShow[key].type === 'documents'
                    ? <i className="material-icons dropdown-icon">picture_as_pdf</i>
                    : <i className="material-icons dropdown-icon">video_library</i>}
                  <p>{(dataToShow[key].name)}{(dataToShow[key].title)}</p>
                </div>
              );
            } else { return (undefined); }
          }) : undefined}
      </div>
    );
  }
}
