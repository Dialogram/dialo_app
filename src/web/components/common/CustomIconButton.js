import React from 'react';
import '../../../css/Component/Component.css';
import '../../../css/index.css';

export class CustomIconButton extends React.Component {
  render() {
    return (
      <div className="custom-button-root-container" onClick={() => { this.props.onClickElement(); }}>
        <div className="custom-button-container">
          <i className={this.props.iconClass}>{this.props.iconName}</i>
          <div className="custom-button-label">
            {this.props.label}
          </div>
        </div>
      </div>
    );
  }
}
