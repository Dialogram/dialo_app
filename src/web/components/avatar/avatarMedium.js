import React, { Component } from 'react';
import { connect } from 'react-redux';
import UserHelper from 'flux/User/Selector';

import './avatar.css';

export class AvatarMedium extends Component {
    render() {
        if (this.props.profile === null)
            return null;
        return (
            <div className={"avatar-medium-content"}>
                <img src={this.props.profile.profilePicture.url} />
                <div className={"avatar-medium-identity"}>
                    <h6>{`${this.props.firstName} ${this.props.lastName}`}</h6>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        nickName: UserHelper.getUserNickname(state),
        profile: UserHelper.getUserProfile(state),
        email: UserHelper.getUserEmail(state),
        firstName: UserHelper.getUserFirstName(state),
        lastName: UserHelper.getUserLastName(state)
    };
}

export default connect(mapStateToProps)(AvatarMedium);