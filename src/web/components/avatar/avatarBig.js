import React, { Component } from 'react';
import { connect } from 'react-redux';
import UserHelper from 'flux/User/Selector';

import './avatar.css';

export class AvatarBig extends Component {
    render() {
        if (this.props.profile === null)
            return null;
        return (
            <div className={"avatar-big-content"}>
                <img src={this.props.profile.profilePicture.url} />
                <div className={"avatar-big-identity"}>
                    <h6>{`${this.props.firstName} ${this.props.lastName}`}</h6>
                    <p>{this.props.nickName}</p>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        nickName: UserHelper.getUserNickname(state),
        profile: UserHelper.getUserProfile(state),
        email: UserHelper.getUserEmail(state),
        firstName: UserHelper.getUserFirstName(state),
        lastName: UserHelper.getUserLastName(state)
    };
}

export default connect(mapStateToProps)(AvatarBig);