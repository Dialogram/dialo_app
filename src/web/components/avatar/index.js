import AvatarBig from './avatarBig';
import AvatarMedium from './avatarMedium';
export {
    AvatarBig,
    AvatarMedium
}