import React, { Component } from 'react';

import ExplorerBar from './ExplorerBar';
import ExplorerTool from './ExplorerTool';
import DocumentGalierie from './DocumentGalerie';
import '../../../css/explorer.css';

export class Explorer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      config: {
        category: 'business',
        field: 'name',
        sort: 'asc',
        limit: '50',
        page: '1',
      },
    };
  }

    onChangeNext = () => {
      let conf = this.state.config;
      let p = parseInt(conf.page, 10) + 1;
      conf.page = `${p}`;
      this.setState({
        ...this.state,
        config: conf,
      });
    }
    onChangePrev = () => {
      let conf = this.state.config;
      let p = parseInt(conf.page, 10) - 1;
      conf.page = `${p >= 0 ? p : 0}`;
      this.setState({
        ...this.state,
        config: conf,
      });
    }
    onConfigChange = (config) => {
      let conf = { ...this.state.config, ...config };
      this.setState({
        ...this.state,
        config: conf,
      }, () => {
        console.log('CHANGE CONFIG', config, this.state);
      });
    }

    render() {
      return (
        <div className={'explorer-container'}>
          <ExplorerBar />
          <ExplorerTool onConfigChange={this.onConfigChange} selected={this.state.config.category}/>
          <DocumentGalierie category={this.state.config.category} field={this.state.config.field}
            sort={this.state.config.sort}
            limit= {this.state.config.limit}
            page= {this.state.config.page}
            next={this.onChangeNext}
            prev = {this.onChangePrev} />
        </div>);
    }
}

export default Explorer;
