import React, { Component } from 'react';

export default class ExplorerTool extends Component {
  render() {
    return (<div className={'explorer-tool'}>
      <h4 className={'explorer-tool-cat'}>Catégories</h4>
      <div className={this.props.selected === 'health' ? 'explorer-tool-item explorer-tool-item-selected' : 'explorer-tool-item'} onClick={(e) => {
        this.props.onConfigChange({ category: 'health' });
      }}>
        <i className="material-icons">healing</i>
        <span className={'explorer-tool-item-name'}>Santé</span>

      </div>

      <div className={this.props.selected === 'business' ? 'explorer-tool-item explorer-tool-item-selected' : 'explorer-tool-item'} onClick={(e) => {
        this.props.onConfigChange({ category: 'business' });
      }}>
        <i className="material-icons">business</i>
        <span className={'explorer-tool-item-name'}>Affaire</span>

      </div>

      <div className={this.props.selected === 'entertainment' ? 'explorer-tool-item explorer-tool-item-selected' : 'explorer-tool-item'} onClick={(e) => {
        this.props.onConfigChange({ category: 'entertainment' });
      }}>
        <i className="material-icons">games</i>
        <span className={'explorer-tool-item-name'}>Divertissement</span>

      </div>
      <div className={this.props.selected === 'administrative' ? 'explorer-tool-item explorer-tool-item-selected' : 'explorer-tool-item'} onClick={(e) => {
        this.props.onConfigChange({ category: 'administrative' });
      }}>
        <i className="material-icons">attach_file</i>
        <span className={'explorer-tool-item-name'}>Administratif</span>

      </div>
      <div className={this.props.selected === 'finance' ? 'explorer-tool-item explorer-tool-item-selected' : 'explorer-tool-item'} onClick={(e) => {
        this.props.onConfigChange({ category: 'finance' });
      }}>
        <i className="material-icons">euro_symbol</i>
        <span className={'explorer-tool-item-name'}>Finance</span>

      </div>
      <h4 className={'explorer-tool-cat'}>Filtre</h4>
      <div className={'explorer-tool-item'} onClick={(e) => {
        this.props.onConfigChange({ field: 'name' });
      }}>
        <i className="material-icons">sort_by_alpha</i>
        <span className={'explorer-tool-item-name'}>Ordre Alphabétique</span>
      </div>
      <div className={'explorer-tool-item'} onClick={(e) => {
        this.props.onConfigChange({ field: 'creationDate' });
      }}>
        <i className="material-icons">sort</i>
        <span className={'explorer-tool-item-name'}>Date de creation</span>

      </div>
    </div>);
  }
}
