import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import NavigationFlux from '../../../flux/navigation.web';
import { exploreDocument } from '../../../flux/Documents/Actions';
import DocumentHelper from '../../../flux/Documents/Selector';

export class DocumentGalerie extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      config: {
        category: 'health',
        field: 'name',
        sort: 'asc',
        limit: '1',
        page: '1',
      },
    };
  }

  static getDerivedStateFromProps(props, state) {
    if (props.category !== state.config.category || props.page !== state.config.page || props.field !== state.config.field) {
      props.exploreDocument(props);
      return { ...state, config: { ...state.config, category: props.category, page: props.page, field: props.field } };
    }
    return state;
  }

    cutDesc = (str) => {
      return str.substring(1, 20) + '...';
    }

    render() {
      const DocList = () => {
        let array = [];
        for (let a in this.props.getExplorerDocumentByCat) {
          array.push(
            <div key={a} className="explorer-box-item" onClick={() => {
              NavigationFlux.navigate(`/traduction/view/${this.props.getExplorerDocumentByCat[a].id}`, 'replace');
            }}>
              <i className="material-icons explorer-icon">picture_as_pdf</i>
              <span className="explorer-box-item-name">{this.props.getExplorerDocumentByCat[a].name}</span>
              <span className="explorer-box-item-desc">{this.cutDesc(this.props.getExplorerDocumentByCat[a].description)}</span>
              <span className="explorer-box-item-desc">{this.props.getExplorerDocumentByCat[a].nbPage} {this.props.getExplorerDocumentByCat[a].nbPage === 1 ? 'page' : 'pages'}</span>
            </div>
          );
        }
        if (array.length !== 0) { return array; } else if (this.props.getPullDocumentStatus.status === 'Loading') {
          return <div className="explorer-box-item" >    Loading  </div>;
        } else if (this.props.getPullDocumentStatus.status === 'Success') { return <div className="explorer-box-item " > Aucun document trouvé  </div>; } else if (this.props.getPullDocumentStatus.status === 'Failure') { return <div className="explorer-box-item" > Aucun document trouvé  </div>; }
      };
      return (
        <div className={'explorer-box'}>
          <div className={'explorer-box-item-box'}> {DocList()}</div>
          <div className={'explorer-page'}>
            <div onClick={this.props.prev} className={'explorer-page-item'}>
              <i className="material-icons">keyboard_arrow_left</i>
            </div>
            <div className={'explorer-page-item'}> {this.state.config.page} </div>
            <div onClick={this.props.next} className={'explorer-page-item'}>
              <i className="material-icons">keyboard_arrow_right</i>
            </div>
          </div>
        </div>
      );
    }
}

const mapStateToProps = (state, ownProps) => {
  return {
    getExploreDocument: DocumentHelper.getExploreDocument(),
    getExplorerDocumentByCat: DocumentHelper.getExplorerDocumentByCat(ownProps.category),
    getPullDocumentStatus: DocumentHelper.getPullDocumentStatus(),
  };
};

const mapDispatchToPropsTransient = (dispatch) => {
  return {
    exploreDocument: bindActionCreators(exploreDocument, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToPropsTransient)(DocumentGalerie);
