import React, { Component } from 'react';

import NavigationFlux from '../../flux/navigation.web';

import AccessRight from './AccessRight';
import { CustomIconButton } from './common/CustomIconButton';
import Confirmation from './Confirmation';

import EditDocumentDetails from './EditDocumentDetails';
import PopupWindow from './PopupWindow';

export default class CreateDocumentPanel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      deleteDocument: false,
      popUpEditDocument: false,
      creatingZone: false,
      popupAccessRight: false,
    };
  }

    updateDoc = () => {
      /* if (this.props.document.access === undefined) { return null; }
        if (this.props.document.access.length !== 0) { */
      return (
        <div>

          {/*
            <div>
              <CustomIconButton
                onClickElement={this.openDocumentDetails}
                iconClass="material-icons icon-trad"
                iconName="edit"
                label="Modifier ce document"/>

              <CustomIconButton
                onClickElement={this.openConfirmationBox}
                iconClass="material-icons icon-trad"
                iconName="delete_outline"
                label="Supprimer ce document"/>
            </div>
  */}

          <div>
            <CustomIconButton
              onClickElement={() => {
                NavigationFlux.navigate(`/traduction/view/${this.props.docId}`, 'replace');
              }}
              iconClass="material-icons icon-trad"
              iconName="done_all"
              label="Terminer la traduction"/>

            {
              this.props.document.access.role !== 'collaborator' &&
                    <CustomIconButton
                      onClickElement={this.onRightPopupOpen}
                      iconClass="material-icons icon-trad"
                      iconName="build"
                      label="Gérer les droits du document"/>
            }
          </div>
          <div className="panel-row-container">
            <CustomIconButton
              onClickElement={this.props._isCreatingZone}
              iconClass="material-icons icon-trad"
              iconName="photo_size_select_small"
              label="Créer une zone à traduire"/>

            <CustomIconButton
              onClickElement={this.props._showVideoList}
              iconClass="material-icons icon-trad"
              iconName="link"
              label="Lier cette zone à une vidéo"/>

            <CustomIconButton
              onClickElement={this.openDeleteZone}
              iconClass="material-icons icon-trad"
              iconName="delete_outline"
              label="Supprimer cette zone"/>
          </div>
        </div>
      );
      // }
    };

    onRightPopupClose = (e) => {
      this.setState({
        ...this.state,
        popupAccessRight: false,
      });
      e.preventDefault();
    };

      onRightPopupOpen = () => {
        this.setState({
          ...this.state,
          popupAccessRight: true,
        });
      };

    openDocumentDetails = () => {
      this.setState({
        popUpEditDocument: true,
      });
    };
    closeEditDocument = () => {
      this.setState({
        popUpEditDocument: false,
      });
    };

    openConfirmationBox = () => {
      this.setState({
        deleteDocument: true,
      });
    };

    _handleDeleteDocument = () => {
      this.props.deleteDocumentFlow(this.props.docId);
      this.closeValidateDeleteDoc();
    };

    closeValidateDeleteDoc = () => {
      this.setState({
        deleteDocument: false,
      });
    };

    closeDeleteZone = () => {
      this.setState({
        deleteZone: false,
      });
    };
    openDeleteZone = () => {
      this.setState({
        ...this.state,
        deleteZone: true,
      });
    };
    render() {
      if (this.props.document === null) {
        return null;
      }
      if (this.props.document.access.role !== 'owner' && this.props.document.access.role !== 'moderator' && this.props.document.access.role !== 'collaborator') {
        return null;
      }
      return (
        <div className={'traduction-toolbar-item'}>
          <PopupWindow
            title={'Gérer les droits'}
            close={this.onRightPopupClose}
            isOpen={this.state.popupAccessRight}>
            <AccessRight
              doc={this.props.document}/>
          </PopupWindow>

          <PopupWindow title={'Modifier les détails d\'un document'} close={this.closeEditDocument}
            isOpen={this.state.popUpEditDocument}>
            <EditDocumentDetails document={this.props.document}/>
          </PopupWindow>

          <PopupWindow
            title={'Supprimer une zone'}
            close={this.closeDeleteZone}
            isOpen={this.state.deleteZone}>
            <Confirmation
              boxTitle={'Êtes-vous sûr de vouloir supprimer cette zone ?'}
              deletionConfirmed={() => {
                this.props._handleDeleteZone();
                this.closeDeleteZone();
              }}
              deletionCanceled={this.closeDeleteZone}/>
          </PopupWindow>

          <PopupWindow
            title={'Supprimer un document'}
            close={this.closeValidateDeleteDoc}
            isOpen={this.state.deleteDocument}>
            <Confirmation
              boxTitle={'Êtes-vous sûr de vouloir supprimer ce document ?'}
              deletionConfirmed={this._handleDeleteDocument}
              deletionCanceled={this.closeValidateDeleteDoc}/>
          </PopupWindow>

          {this.updateDoc()}
        </div>
      );
    }
}
