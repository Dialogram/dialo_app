import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import '../../css/header/header.css';
import { Logout } from '../../flux/baseAction';
import NavigationFlux from '../../flux/navigation.web';
import SessionHelper from '../../flux/Session/Selector';
import UserHelper from '../../flux/User/Selector';
import PopupWindow from './PopupWindow';
import CreateTicket from './ticket/CreateTicket';

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchText: '',
      isOpen : false
    };

    this._onClickSearch = this._onClickSearch.bind(this);
    this._handleInputChange = this._handleInputChange.bind(this);
    this._handleEnterKeyPress = this._handleEnterKeyPress.bind(this);
    this._signOut = this._signOut.bind(this);
  }

  onClickProfileImage() {
    NavigationFlux.navigate('/settings', 'replace');
  }

  closeAddPdf = () =>{
    this.setState({
      ...this.state,
      isOpen : false
    })
  }
  render() {
    if (this.props.sessionActive === false) {
      return null;
    }
    return (
      <header>
        <PopupWindow title={'Support'} close={this.closeAddPdf} isOpen={this.state.isOpen}>
            <CreateTicket /></PopupWindow>
        <div className="root-header-container">
          <div className="header-container">
            <div className="logo-title-header-container">
              <a href="/" className="logo-title-header-link">
                <div className="logo-header-container">
                  <img
                    alt={'logo'}
                    className={'header-logo'}
                    src={require('image/2020_logo_DIALOGRAM.png')}
                  />
                </div>
                <div className="logo-title-header-separator" />
                <div className="header-title-container">
                  Dialogram
                </div>
              </a>
            </div>

            <div className="icon-header-container">
              <div className="profile-icon-container">
                {/* <img
                  alt={'notifications'}
                  className={'notification-icon'}
                  src={require('image/notif-icon.png')} /> */}
                  <a onClick={(e)=>{
                    this.setState({
                      ...this.state,
                      isOpen : true
                    })
                  }}>Support</a>
                <img
                  onClick={this.onClickProfileImage}
                  alt={'profile'}
                  className={'profile-icon'}
                  src={this.props.pictureUrl ? this.props.pictureUrl : require('image/profile-icon.png')} />
              </div>
              <div className="logo-title-header-separator" />
              <div className="header-logout-container">
                <a className="logout-header-link" onClick={this._signOut}>Déconnexion</a>
              </div>
            </div>
          </div>
        </div>
      </header>
    );
  }
/*

            <div className="searchbar-header-container">
              <input
                value={this.state.searchText}
                onKeyPress={this._handleEnterKeyPress}
                onChange={this._handleInputChange}
                type="text"
                name="searchText"
                placeholder="Rechercher"
                className="searchbar-input-header" />
              <img
                onClick={this._onClickSearch}
                alt={'search'}
                className={'search-icon'}
                src={require('image/search-icon.png')} />
            </div>
            <div className="header-explore-container" onClick={(e) => {
              NavigationFlux.navigate(`/explorer`, 'replace');
            }}>
              <p className="header-label-explore">Explore</p>
            </div>

*/
  _onClickSearch() {
    if (this.state.searchText !== null && this.state.searchText !== undefined) {
      NavigationFlux.navigate(`/search/${this.state.searchText}`, 'replace');
    }
  }

  _handleInputChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  };

  _handleEnterKeyPress(e) {
    if (e.key === 'Enter' && this.state.searchText !== null && this.state.searchText !== undefined) {
      NavigationFlux.navigate(`/search/${this.state.searchText}`, 'replace');
    }
  }

  _signOut(event) {
    this.props.Logout();
    event.preventDefault();
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    sessionActive: SessionHelper.hasSessionActive(),
    pictureUrl: UserHelper.getUserprofilePictureUrl(state),
  };
};

const mapDispatchToPropsTransient = (dispatch) => {
  return {
    Logout: bindActionCreators(Logout, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToPropsTransient)(Header);
