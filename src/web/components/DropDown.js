import React, { Component } from 'react';
import '../../css/Component/Component.css';
import '../../css/index.css';

export class DropDown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
    };
    this.spoil = this.spoil.bind(this);
  }

  spoil(e) {
    this.setState({
      ...this.state,
      isOpen: !this.state.isOpen,
    });
    e.preventDefault();
  }

  render() {
    const { title, dataToShow } = this.props;

    return (
      <div className={'dropdown-container'}>
        <div className={'dropdown-header'} onClick={this.spoil}>
          <div className="dropdown-icon-container">
            <i className="material-icons">{this.state.isOpen ? 'keyboard_arrow_down' : 'keyboard_arrow_right'}</i>
          </div>
          <div className="dropdown-title-container">
            <p className="dropdown-title">{title}</p>
          </div>
        </div>
        {
          (dataToShow !== undefined && dataToShow !== null) ? this.state.isOpen ? Object.keys(dataToShow).map((key) => {
            if (dataToShow[key].id) {
              return (
                <div className={'dropdown-item'} key={key} onClick={() => {
                  this.props.onClickElement(dataToShow[key].id);
                }}>
                  {dataToShow[key].type === 'documents'
                    ? <i className="material-icons dropdown-icon">picture_as_pdf</i>
                    : <i className="material-icons dropdown-icon">video_library</i>}
                  <p>{(dataToShow[key].name)}{(dataToShow[key].title)}</p>
                </div>
              );
            } else { return (undefined); }
          }) : undefined : undefined}
      </div>
    );
  }
}
