import React, { Component } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { searchUserFlow } from '../../flux/Search/Actions';
import SearchHelper from '../../flux/Search/Selector';
import { CustomIconButton } from './common/CustomIconButton';
import { giveEditPermission, removeEditPermission, promoteUser, demoteUser } from '../../flux/Documents/Actions';

export class AccessRight extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: '',
      selectedUser: null,
    };
  }

  _handleChange = (e) => {
    const { name, value } = e.target;
    clearTimeout(this.state.interval);
    this.setState({
      [name]: value,
      interval: setTimeout(() => { this.props.searchUserFlow({ toFind: this.state.userName }); }, 300),
    });
  }

  render() {
    const DocList = () => {
      let array = [];
      for (let a in this.props.getUserList) {
        array.push(
          <div className={'userlist-item'} key={a} onClick={() => {
            this.setState({ selectedUser: this.props.getUserList[a] });
          }}>
            <img
              alt={'profile'}
              className={null}
              src={this.props.getUserList[a].profile.profilePicture.url}
            />
            <div>{this.props.getUserList[a].nickName}</div>

          </div>
        );
      }
      return array;
    };
    if (this.state.selectedUser === null) {
      return (<div className="access-right-component-container">
        <input value={this.state.userName}
          className="access-right-component-search-item"
          type="text"
          name="userName"
          placeholder="Rechercher un utilisateur"
          onChange={this._handleChange} />
        <div className="access-right-component-search-item">
          {DocList()}</div>
      </div>);
    } else {
      return (<div className="access-right-component-container">
        <div className="access-right-component-search-item">{this.state.selectedUser.nickName}</div>
        <div className="access-right-component-search-item">
          <CustomIconButton
            onClickElement={() => {
              // this.props.giveEditPermission(this.props.doc, this.state.selectedUser.id);
              this.props.promoteUser({ idUser: this.state.selectedUser.id, role: "moderator", idDocument: this.props.doc.id })
            }}
            iconClass="material-icons icon-trad"
            iconName="photo_size_select_small"
            label="Promouvoir modérateur" />
          <CustomIconButton
            onClickElement={() => {
              // this.props.giveEditPermission(this.props.doc, this.state.selectedUser.id);
              this.props.promoteUser({ idUser: this.state.selectedUser.id, role: "collaborator", idDocument: this.props.doc.id })
            }}
            iconClass="material-icons icon-trad"
            iconName="photo_size_select_small"
            label="Promouvoir collaborateur" />

          <CustomIconButton
            onClickElement={() => {
              this.props.demoteUser({ idUser: this.state.selectedUser.id, role: "collaborator", idDocument: this.props.doc.id })
            }}
            iconClass="material-icons icon-trad"
            iconName="remove_circle_outline"
            label="Retirer les droits de modification" />
        </div>
      </div>);
    }
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    getUserList: SearchHelper.getUserList(),
  };
};

const mapDispatchToPropsTransient = (dispatch) => {
  return {
    searchUserFlow: bindActionCreators(searchUserFlow, dispatch),
    giveEditPermission: bindActionCreators(giveEditPermission, dispatch),
    removeEditPermission: bindActionCreators(removeEditPermission, dispatch),

    promoteUser: bindActionCreators(promoteUser, dispatch),
    demoteUser: bindActionCreators(demoteUser, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToPropsTransient)(AccessRight);
