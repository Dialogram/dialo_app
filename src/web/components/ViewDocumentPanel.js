import React, { Component } from 'react';
import { CustomIconButton } from './common/CustomIconButton';

import NavigationFlux from '../../flux/navigation.web';

import EditDocumentDetails from './EditDocumentDetails';
import PopupWindow from './PopupWindow';
import Confirmation from './Confirmation';

export default class ViewDocumentPanel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      deleteDocument: false,
      popUpEditDocument: false,
    };
  }
    translation = () => {
      /*  if (this.props.document.access === undefined) { return null; }

          if (this.props.document.access.length === 0) { return; } */

      if (this.props.document === null) { return null; }
      if (this.props.document.access.role !== 'owner') {
        return null;
      }
      if (this.props.document.idTranslation === null) {
        return (
          <CustomIconButton
            onClickElement={() => {
              this.props.createDocumentTranslationFlow({ id: this.props.document.id });
              NavigationFlux.navigate(`/traduction/create/${this.props.document.id}`, 'replace');
            }}
            iconClass="material-icons icon-trad"
            iconName="photo_size_select_small"
            label="Créer une traduction" />
        );
      } else {
        return (
          <CustomIconButton
            onClickElement={() => {
              NavigationFlux.navigate(`/traduction/create/${this.props.document.id}`, 'replace');
            }}
            iconClass="material-icons icon-trad"
            iconName="photo_size_select_small"
            label="Reprendre la traduction" />
        );
      }
    };

    updateDoc = () => {
      if (this.props.document === null) { return null; }
      if (this.props.document.access.role !== 'owner' || this.props.document.access.role === 'moderator') {
        return null;
      }
      return (
        <div>
          <CustomIconButton
            onClickElement={this.openDocumentDetails}
            iconClass="material-icons icon-trad"
            iconName="edit"
            label="Modifier ce document" />

          <CustomIconButton
            onClickElement={this.openConfirmationBox}
            iconClass="material-icons icon-trad"
            iconName="delete_outline"
            label="Supprimer ce document" />

        </div>
      );
      // }
    };

    openDocumentDetails = () => {
      this.setState({
        popUpEditDocument: true,
      });
    };
    closeEditDocument = () => {
      this.setState({
        popUpEditDocument: false,
      });
    };

    openConfirmationBox = () => {
      this.setState({
        deleteDocument: true,
      });
    };

    _handleDeleteDocument = () => {
      this.props.deleteDocumentFlow(this.props.docId);
      this.closeValidateDeleteDoc();
    };

    closeValidateDeleteDoc = () => {
      this.setState({
        deleteDocument: false,
      });
    };
    render() {
      return (
        <div className={'traduction-toolbar-item'}>

          <PopupWindow title={'Modifier les détails d\'un document'} close={this.closeEditDocument}
            isOpen={this.state.popUpEditDocument}>
            <EditDocumentDetails document={this.props.document} />
          </PopupWindow>

          <PopupWindow
            title={'Supprimer un document'}
            close={this.closeValidateDeleteDoc}
            isOpen={this.state.deleteDocument}>
            <Confirmation
              boxTitle={'Êtes-vous sûr de vouloir supprimer ce document ?'}
              deletionConfirmed={this._handleDeleteDocument}
              deletionCanceled={this.closeValidateDeleteDoc} />
          </PopupWindow>

          {this.translation()}
          {this.updateDoc()}
        </div>
      );
    }
}
