import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { uploadVideoTranslationFlow } from '../../flux/ApiVideo/Actions';
import { uploadVideoTrad } from '../../flux/TranslationVideo/Actions';

import { postDocument } from '../../flux/Documents/Actions';
import DocumentHelper from '../../flux/Documents/Selector';
import VideoTranslationHelper from '../../flux/TranslationVideo/Selector';
import FormErrors from '../components/FormErrors';
import Utils from '../helpers/Utils';

class AddFile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      uploadFileType: '',
      uploadFileName: '',
      selectedFile: '',
      uploadFileDesc: '',
      uploadFileCategory: '',
      formErrors: { uploadFileName: '', uploadFileType: '', uploadFileCategory: '' },
      selectedFileForm: '',
      uploadFileNameValid: false,
      uploadFileTypeValid: false,
      uploadFileCategoryValid: false,
      formValid: false,
      loading: false,
      public: false,
    };

    this._onUploadFile = this._onUploadFile.bind(this);
    this._handleChange = this._handleChange.bind(this);
    this._validateField = this._validateField.bind(this);
    this._validateForm = this._validateForm.bind(this);
    this._onBlur = this._onBlur.bind(this);
    this._isPublic = this._isPublic.bind(this);
  }

  uploadStatus = () => {
    if (this.state.loading) {
      if (this.props.videoStatus === 'Success' && this.state.uploadFileType === 'video') {
        return (<div className="upload-file-container">Succès</div>);
      } else if (this.props.documentStatus === 'Success' && this.state.uploadFileType === 'document') {
        return (<div className="upload-file-container">Succès</div>);
      } else if (this.props.videoStatus === 'Loading' || this.props.documentStatus === 'Loading') {
        return (<div className="upload-file-container">Chargement...</div>);
      } else {
        return (<img alt={'upload'} className={'upload-icon'} src={require('image/upload-icon.png')} />);
      }
    }
    return (<img alt={'upload'} className={'upload-icon'} src={require('image/upload-icon.png')} />);
  };

  render() {
    return (
      <div className="addfile-component-container">
        <div className="select-root-container">
          <div className={'profile-upload-container'}>
            <input value={this.state.selectedFile}
              className="file-upload-input"
              type="file" id="selectedFile"
              name="selectedFile"
              onChange={this._handleChange} />
          </div>
          <FormErrors fieldName='selectedFile' message={this.state.selectedFileForm} />
        </div>
        <div className="select-root-container">
          <div className={'profile-upload-container'}>
            <input className="file-name-input"
              value={this.state.uploadFileName}
              name="uploadFileName"
              id={'uploadFileName'}
              type={'text'}
              placeholder={'Nom'}
              onBlur={this._onBlur}
              onChange={this._handleChange} />
          </div>
          <FormErrors fieldName='uploadFileName' message={this.state.formErrors.uploadFileName} />
        </div>
        <div className="select-root-container">
          <div className={'profile-upload-container'}>
            <input className="file-name-input"
              value={this.state.uploadFileDesc}
              name="uploadFileDesc"
              id={'uploadFileDesc'}
              type={'text'}
              placeholder={'Description'}
              onChange={this._handleChange} />
          </div>
        </div>
        <div className="type-root-container">
          <select name="uploadFileType"
            id="uploadFileType"
            className="type-select-container"
            value={this.state.uploadFileType}
            onBlur={this._onBlur}
            onChange={this._handleChange}>
            <option selected value="unknown">Type...</option>
            <option value="document">Document</option>
            <option value="video">Vidéo</option>
          </select>
          <FormErrors fieldName='uploadFileType' message={this.state.formErrors.uploadFileType} />
        </div>
        {this.state.uploadFileType === 'document'
          ? <div className="type-root-container">
            <select name="uploadFileCategory"
              id="uploadFileCategory"
              className="type-select-container"
              value={this.state.uploadFileCategory}
              onBlur={this._onBlur}
              onChange={this._handleChange}>
              <option selected value="unknown">Catégorie...</option>
              <option value="administrative">Administratif</option>
              <option value="entertainment">Divertissement</option>
              <option value="business">Entreprise</option>
              <option value="finance">Finance</option>
              <option value="health">Santé</option>
            </select>
            <FormErrors fieldName='uploadFileCategory' message={this.state.formErrors.uploadFileCategory} />
          </div> : undefined
        }
        <div className="checkbox-root-container">
          <label>
            Public
          </label>
          <input
            name="isPublic"
            type="checkbox"
            onChange={this._isPublic} />
        </div>
        <div className="upload-button-container">
          <div className={'profile-upload-container'} onClick={this._onUploadFile}>
            {this.uploadStatus()}
          </div>
        </div>
      </div>
    );
  }

  _isPublic() {
    this.setState({ public: !this.state.public });
  }

  _onBlur(e) {
    this._validateField(e.target.name, e.target.value);
  }

  _validateField(name, value) {
    let fieldValidationErrors = this.state.formErrors;
    let uploadFileNameValid = this.state.uploadFileNameValid;
    let uploadFileTypeValid = this.state.uploadFileTypeValid;
    let uploadFileCategoryValid = this.state.uploadFileCategoryValid;

    switch (name) {
      case 'uploadFileName':
        uploadFileNameValid = value.length >= 5;
        fieldValidationErrors.uploadFileName = uploadFileNameValid ? '' : Utils.ERRORS.NameLen;
        break;
      case 'uploadFileType':
        uploadFileTypeValid = value.length > 0 && !(value.match('unknown'));
        fieldValidationErrors.uploadFileType = uploadFileTypeValid ? '' : Utils.ERRORS.MissType;
        break;
      case 'uploadFileCategory':
        uploadFileCategoryValid = value.length > 0 && !(value.match('unknown'));
        fieldValidationErrors.uploadFileCategory = uploadFileCategoryValid ? '' : Utils.ERRORS.MissCategory;
        break;
      default:
        break;
    }
    this.setState({
      formErrors: fieldValidationErrors,
      uploadFileNameValid: uploadFileNameValid,
      uploadFileTypeValid: uploadFileTypeValid,
      uploadFileCategoryValid: uploadFileCategoryValid,
    }, this._validateForm);
  }

  _validateForm() {
    this.setState({
      formValid: this.state.uploadFileNameValid &&
        this.state.uploadFileTypeValid && (this.state.uploadFileType === 'document' ? this.state.uploadFileCategoryValid : this.state.uploadFileType === 'video'),
    });
  };

  _onUploadFile(e) {
    this.setState({ selectedFileForm: this.state.selectedFile.length === 0 ? Utils.ERRORS.MissFile : '' });
    if (this.state.formValid === false || this.state.formValid === undefined || this.state.selectedFile.length === 0) { return; }

    this.setState({ loading: true });

    // eslint-disable-next-line no-undef
    let formData = new FormData();
    let imageFile = document.querySelector('#selectedFile');

    if (this.state.uploadFileType === 'document') {
      formData.append('file', imageFile.files[0]);
      formData.append('name', this.state.uploadFileName);
      formData.append('description', this.state.uploadFileDesc);
      formData.append('nbPage', 34);
      formData.append('public', this.state.public);
      formData.append('category', this.state.uploadFileCategory);
      this.props.postDocument(formData);
    } else if (this.state.uploadFileType === 'video') {
      formData.append('video', imageFile.files[0]);
      formData.append('public', this.state.public);
      formData.append('description', this.state.uploadFileDesc);
      formData.append('title', this.state.uploadFileName);
      // this.props.uploadVideoTranslationFlow(formData, this.state.uploadFileName, this.state.uploadFileDesc);
      this.props.uploadVideoTrad({ video: formData })
    }
    e.preventDefault();
  }

  _handleChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  };
}

const mapStateToProps = (state, ownProps) => {
  return {
    documentStatus: DocumentHelper.getDocumentStatus(state),
    videoStatus: VideoTranslationHelper.getVideoTranslationStatus(state),
  };
};

const mapDispatchToPropsTransient = (dispatch) => {
  return {
    postDocument: bindActionCreators(postDocument, dispatch),
    uploadVideoTranslationFlow: bindActionCreators(uploadVideoTranslationFlow, dispatch),

    uploadVideoTrad: bindActionCreators(uploadVideoTrad, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToPropsTransient)(AddFile);
