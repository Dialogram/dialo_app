import React, { Component } from 'react';

class FormErrors extends Component {
  render() {
    if (!(this.props.fieldName && this.props.message)) {
      return null;
    }
    return (
      <div className="form-error-container">
        <span className="span-error">{this.props.message}</span>
      </div>
    );
  }
}

export default FormErrors;
