import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { uploadVideoTranslationFlow } from '../../flux/ApiVideo/Actions';
import { documentPdfUpdate, postDocument } from '../../flux/Documents/Actions';
import DocumentHelper from '../../flux/Documents/Selector';
import VideoHelper from '../../flux/Videos/Selector';

class EditDocumentDetails extends Component {
  constructor(props) {
    super(props);

    this.state = {
      uploadFileName: this.props.document.name,
      uploadFileDesc: this.props.document.description,
      uploadFileCategory: this.props.document.category,
      loading: false,
      public: this.props.document.public,
    };

    this._onUploadFile = this._onUploadFile.bind(this);
    this._handleChange = this._handleChange.bind(this);
    this._isPublic = this._isPublic.bind(this);
  }

  render() {
    return (
      <div className="addfile-component-container">
        <div className="select-root-container">
          <div className={'profile-upload-container'}>
            <input className="file-name-input"
              value={this.state.uploadFileName}
              name="uploadFileName"
              id={'uploadFileName'}
              type={'text'}
              placeholder={this.state.uploadFileName}
              onChange={this._handleChange}/>
          </div>
        </div>
        <div className="select-root-container">
          <div className={'profile-upload-container'}>
            <input className="file-name-input"
              value={this.state.uploadFileDesc}
              name="uploadFileDesc"
              id={'uploadFileDesc'}
              type={'text'}
              placeholder={this.state.uploadFileDesc}
              onChange={this._handleChange}/>
          </div>
        </div>
        <div className="type-root-container">
          <select name="uploadFileCategory"
            id="uploadFileCategory"
            className="type-select-container"
            value={this.state.uploadFileCategory}
            onChange={this._handleChange}>
            <option value="administrative">Administratif</option>
            <option value="entertainment">Divertissement</option>
            <option value="business">Entreprise</option>
            <option value="finance">Finance</option>
            <option value="health">Santé</option>
          </select>
        </div>
        <div className="checkbox-root-container">
          <label>
            Public
          </label>
          <input
            name="isPublic"
            type="checkbox"
            checked={this.state.public}
            onChange={this._isPublic}/>
        </div>
        <div className="upload-button-container">
          <div className={'profile-upload-container'} onClick={this._onUploadFile}>
            {this.state.loading && this.props.documentStatus === 'Loading'
              ? <div className="upload-file-container">Chargement...</div>
              : this.props.documentStatus === 'Success' && this.state.loading
                ? <i className="material-icons icon-trad">done_all</i>
                : <i className="material-icons icon-trad">done</i>
            }
          </div>
        </div>
      </div>
    );
  }

  _isPublic() {
    this.setState({ public: !this.state.public });
  }

  _onUploadFile(e) {
    this.setState({ loading: true });
    let publicDoc = '';
    if (this.state.public) { publicDoc = 'true'; } else { publicDoc = 'false'; }
    this.props.editDocument(
      { id: this.props.document.id },
      {
        name: this.state.uploadFileName,
        description: this.state.uploadFileDesc,
        publicDoc: publicDoc,
        category: this.state.uploadFileCategory,
      });
    e.preventDefault();
  }

  _handleChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  };
}

const mapStateToProps = (state, ownProps) => {
  return {
    documentStatus: DocumentHelper.getDocumentStatus(state),
    videoStatus: VideoHelper.getVideoStatus(state),
  };
};

const mapDispatchToPropsTransient = (dispatch) => {
  return {
    editDocument: bindActionCreators(documentPdfUpdate, dispatch),
    postDocument: bindActionCreators(postDocument, dispatch),
    uploadVideoTranslationFlow: bindActionCreators(uploadVideoTranslationFlow, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToPropsTransient)(EditDocumentDetails);
