import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { deleteAccount } from 'flux/User/Actions';
import UserHelper from 'flux/User/Selector';
import { SUBMIT } from 'web/helpers/Forms';
import { Form } from 'redux-form';

class DeleteAccount extends Component {
  state = {
    stateRequest: 'Default',
  };

  _handleSubmit = e => {
    e.preventDefault();
    this.props.deleteAccount();
  };

  render() {
    return (
      <form className="settingsForm" onSubmit={this._handleSubmit}>
        <div>
          <h2>Suppression de compte</h2>
          {this.state.stateRequest === 'Default' && this.props.requestStatus.status !== 'Default' &&
          <strong>{this.props.requestStatus.status}</strong>}
        </div>
        <div className="saveDiv">
          <button type="submit" className="saveButton">
            {SUBMIT}
          </button>
        </div>
      </form>
    );
  }
}

function mapStateToProps(state) {
  return {
    requestStatus: UserHelper.getDeleteAccountStatus(state),
  };
}

const mapDispatchToPropsTransient = (dispatch) => {
  return {
    deleteAccount: bindActionCreators(deleteAccount, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToPropsTransient)(DeleteAccount);
