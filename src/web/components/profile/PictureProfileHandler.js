import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { updateProfilePicture } from 'flux/User/Actions';
import UserHelper from 'flux/User/Selector';
import { SUBMIT } from 'web/helpers/Forms';
import { Form } from 'redux-form';

class PictureProfileHandler extends Component {
  state = {
    file: '',
    imagePreviewUrl: '',
    stateRequest: 'Default',
  };

  _handleSubmit = e => {
    e.preventDefault();
    if (this.state.file) {
      let data = new FormData();
      data.append('image', this.state.file);
      this.props.updateProfilePicture(data);
    }
  };

  _handleImageChange = e => {
    e.preventDefault();

    let reader = new FileReader();
    let file = e.target.files[0];
    reader.onloadend = () => {
      this.setState({ file: file, imagePreviewUrl: reader.result });
    };
    reader.readAsDataURL(file);
  };

  render() {
    let { imagePreviewUrl } = this.state;
    let { pictureUrl } = this.props;
    let $imagePreview = null;
    if (imagePreviewUrl) {
      $imagePreview = (<img className="imgPreview" alt="preview" src={imagePreviewUrl}/>);
    } else {
      $imagePreview = (pictureUrl && <img className="imgActual" alt="actual" src={pictureUrl}/>);
    }

    return (
      <form className="settingsForm" onSubmit={this._handleSubmit}>
        <input type="file" onChange={this._handleImageChange}/>
        <div>
          {$imagePreview}
          {this.state.stateRequest === 'Default' && this.props.requestStatus.status !== 'Default' &&
          <strong>{this.props.requestStatus.status}</strong>}
        </div>
        <div className="saveDiv">
          <button type="submit" className="saveButton">
            {SUBMIT}
          </button>
        </div>
      </form>
    );
  }
}

function mapStateToProps(state) {
  return {
    pictureUrl: UserHelper.getUserprofilePictureUrl(state),
    requestStatus: UserHelper.getUpdateAccountStatus(state),
  };
}

const mapDispatchToPropsTransient = (dispatch) => {
  return {
    updateProfilePicture: bindActionCreators(updateProfilePicture, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToPropsTransient)(PictureProfileHandler);
