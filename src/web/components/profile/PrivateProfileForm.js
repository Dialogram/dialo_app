import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Form, Field, reduxForm } from 'redux-form';

import { updateUserPrivate } from 'flux/User/Actions';
import UserHelper from 'flux/User/Selector';
import { SUBMIT, maxLength22, minLength5, renderField, required, email } from 'web/helpers/Forms';

const form = reduxForm({
  form: 'PrivateProfileForm',
  enableReinitialize: true,
});

const LABEL = {
  'nickName': 'Pseudo',
  'email': 'Email',
};

class PrivateProfileForm extends Component {
  state = {
    stateRequest: 'Default',
  };

  handlePrivateSubmit(formProps) {
    if (this.props.nickName !== formProps.nickName || this.props.email !== formProps.email) {
      this.props.updateUserPrivate(formProps);
    }
  }

  render() {
    const { handleSubmit, submitting } = this.props;
    return (
      <Form className="settingsForm" onSubmit={handleSubmit(values => this.handlePrivateSubmit(values))}>
        <Field
          name="nickName" type="text" component={renderField}
          label={LABEL.nickName} placeholder=""
          validate={[required, minLength5, maxLength22]}/>
        <Field
          name="email" type="email" component={renderField}
          label={LABEL.email} placeholder=""
          validate={[required, email]}/>
        {this.state.stateRequest === 'Default' && this.props.requestStatus.status !== 'Default' &&
        <strong>{this.props.requestStatus.status}</strong>}
        <div className="saveDiv">
          <button type="submit" className="saveButton" disabled={submitting}>
            {SUBMIT}
          </button>
        </div>
      </Form>
    );
  }
}

function mapStateToProps(state) {
  return {
    initialValues: {
      nickName: UserHelper.getUserNickname(state),
      email: UserHelper.getUserEmail(state),
    },
    requestStatus: UserHelper.getUpdateAccountStatus(state),
  };
}

const mapDispatchToPropsTransient = (dispatch) => {
  return {
    updateUserPrivate: bindActionCreators(updateUserPrivate, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToPropsTransient)(form(PrivateProfileForm));
