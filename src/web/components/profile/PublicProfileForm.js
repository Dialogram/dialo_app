import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Form, Field, reduxForm } from 'redux-form';

import { updateUserPublic } from 'flux/User/Actions';
import UserHelper from 'flux/User/Selector';
import { SUBMIT, maxLength22, minLength3, renderField, required, genderField } from 'web/helpers/Forms';

const form = reduxForm({
  form: 'PublicProfileForm',
  enableReinitialize: true,
});

const LABEL = {
  'firstName': 'Prénom',
  'lastName': 'Nom',
  'birthday': 'Date de naissance',
  'gender': 'Sexe',
  'male': 'Homme',
  'female': 'Femme',
  'country': 'Pays',
  'hometown': 'Ville',
  'description': 'Description',
};

class PublicProfileForm extends Component {
  state = {
    stateRequest: 'Default',
  };

  handlePublicSubmit(formProps) {
    if (this.props.firstName !== formProps.firstName || this.props.lastName !== formProps.lastName) {
      this.props.updateUserPublic(formProps);
    }
  }

  render() {
    const { handleSubmit, submitting } = this.props;
    return (
      <Form className="settingsForm" onSubmit={handleSubmit(values => this.handlePublicSubmit(values))}>
        <Field
          name="firstName" type="text" component={renderField}
          label={LABEL.firstName} placeholder=""
          validate={[required, minLength3, maxLength22]}/>
        <Field
          name="lastName" type="text" component={renderField}
          label={LABEL.lastName} placeholder=""
          validate={[required, minLength3, maxLength22]}/>
        <Field name="birthday" type="date" component={renderField}
          label={LABEL.birthday} placeholder=""/>
        <div className="fieldSettings">
          <legend className="labelSettings">{LABEL.gender}</legend>
          <Field name="gender" type="radio" component={genderField}
            label={LABEL.male} placeholder="" value="male"/>
          <Field name="gender" type="radio" component={genderField}
            label={LABEL.female} placeholder="" value="female"/>
        </div>
        <Field name="country" type="text" component={renderField}
          label={LABEL.country} placeholder=""/>
        <Field name="hometown" type="text" component={renderField}
          label={LABEL.hometown} placeholder=""/>
        <Field name="description" type="text" component={renderField}
          label={LABEL.description} placeholder=""/>
        {this.state.stateRequest === 'Default' && this.props.requestStatus.status !== 'Default' &&
        <strong>{this.props.requestStatus.status}</strong>}
        <div className="saveDiv">
          <button type="submit" className="saveButton" disabled={submitting}>
            {SUBMIT}
          </button>
        </div>
      </Form>
    );
  }
}

function mapStateToProps(state) {
  return {
    initialValues: {
      birthday: UserHelper.getUserProfile().birthday,
      country: UserHelper.getUserProfile().country,
      description: UserHelper.getUserProfile().description,
      firstName: UserHelper.getUserProfile(state).firstName,
      gender: UserHelper.getUserProfile().gender,
      hometown: UserHelper.getUserProfile().hometown,
      lastName: UserHelper.getUserProfile(state).lastName,
    },
    requestStatus: UserHelper.getUpdateAccountStatus(state),
  };
}

const mapDispatchToPropsTransient = (dispatch) => {
  return {
    updateUserPublic: bindActionCreators(updateUserPublic, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToPropsTransient)(form(PublicProfileForm));
