import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Form, Field, reduxForm } from 'redux-form';

import { updatePassword } from 'flux/User/Actions';
import UserHelper from 'flux/User/Selector';
import { SUBMIT, renderField, required } from 'web/helpers/Forms';

const form = reduxForm({
  form: 'PasswordForm',
  enableReinitialize: true,
});

const LABEL = {
  'currentPassword': 'Mot de passe actuel',
  'newPassword': 'Nouveau mot de passe',
  'newPassword_confirm': 'Confirmez le mot de passe rentré précédemment',
};

class PasswordForm extends Component {
  state = {
    stateRequest: 'Default',
  };

  handlePasswordSubmit(formProps) {
    if (formProps.newPassword === formProps.newPassword_confirm) {
      this.props.updatePassword(formProps);
    }
  }

  render() {
    const { handleSubmit, submitting } = this.props;
    return (
      <Form className="settingsForm" onSubmit={handleSubmit(values => this.handlePasswordSubmit(values))}>
        <Field
          name="currentPassword" type="password" component={renderField}
          label={LABEL.currentPassword} placeholder=""
          validate={[required]}/>
        <Field
          name="newPassword" type="password" component={renderField}
          label={LABEL.newPassword} placeholder=""
          validate={[required]}/>
        <Field
          name="newPassword_confirm" type="password" component={renderField}
          label={LABEL.newPassword_confirm} placeholder=""
          validate={[required]}/>
        {this.state.stateRequest === 'Default' && this.props.requestStatus.status !== 'Default' &&
        <strong>{this.props.requestStatus.status}</strong>}
        <div className="saveDiv">
          <button type="submit" className="saveButton" disabled={submitting}>
            {SUBMIT}
          </button>
        </div>
      </Form>
    );
  }
}

function mapStateToProps(state) {
  return {
    initialValues: {
      currentPassword: '',
      newPassword: '',
      newPassword_confirm: '',
    },
    requestStatus: UserHelper.getUpdatePasswordStatus(state),
  };
}

const mapDispatchToPropsTransient = (dispatch) => {
  return {
    updatePassword: bindActionCreators(updatePassword, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToPropsTransient)(form(PasswordForm));
