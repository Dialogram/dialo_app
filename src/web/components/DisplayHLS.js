import React, { Component } from 'react';
import { connect } from 'react-redux';
import ReactPlayer from 'react-player';
import VideoTranslationHelper from '../../flux/TranslationVideo/Selector';

class DisplayHLS extends Component {
  render() {
    return (
      <div>
        <div className="react-player-container">
          <ReactPlayer url={this.props.video.assets.hls} controls width="395px" height="260px" playing={true} loop={true} />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    video: VideoTranslationHelper.getVideoTranslationById({ id: ownProps.selectedVideo }),
  };
};

const mapDispatchToPropsTransient = (dispatch) => {
  return {
  };
};

export default connect(mapStateToProps, mapDispatchToPropsTransient)(DisplayHLS);
