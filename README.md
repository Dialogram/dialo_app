# dialo_app


# Dialogram

Dialogram is a collaborative platform, available on mobile and web, which aims to provide interpretations in French Sign Language (LSF) of media and documents.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

* [NodeJS](https://nodejs.org/en/download/) - JavaScript runtime built on Chrome's V8 JavaScript engine.
* [Yarn](https://yarnpkg.com/lang/en/docs/install/) - FAST, RELIABLE, AND SECURE DEPENDENCY MANAGEMENT.

### Installing


At the root of the project install all the dependencies

```
yarn install
```
```
git submodule update --remote
```
```
cd src/flux
```
```
git checkout master
```
```
git pull
```
## Running the app

```
yarn start
```

## Documentation

Please read the [documentation](https://dialogram.github.io/dialo_back_doc/) of the project.

## Running the tests

Not available.

## Deployment

Not available.

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

Not available.

## Authors

* **Dialogram Team** - [Members](https://gitlab.com/groups/Dialogram/-/group_members)

See also the list of [contributors](https://gitlab.com/Dialogram/dialo_back/graphs/master) who participated in this project.

## Main Web Developpers

* [François Bêche](https://gitlab.com/Bronovitch)
* [César Baud](https://gitlab.com/baudc)
* [Loik Faure-Berlinguette](https://gitlab.com/loikfb)
* [Robin Kerdiles](https://gitlab.com/Dr-Shadow)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details


[MobileApp Screenshots](./src/mobile/README.md)